﻿public enum PalID
{
    //SPROUTS
    SPROUTA,
    SPROUTB,
    SPROUTC,
    SPROUTD
}

public enum PalAges
{
    EGG = 0,

    SPROUT,
    YOUNGIN,
    ADULT,
    OLDIE,
    
    DEAD
}

public enum ItemID
{
    NONE = 0,
    
    // Materials
    WOOD,
    STONE,
    IRON,
    CLAY,
    LEAVES,

    // Food
    FRUIT,

    // Specials
    SPECIAL_RAREITEMS,
    SPECIAL_MOREIEMS,
    SPECIAL_SLOWSTATS,
    SPECIAL_STAMINA,
    SPECIAL_FULL,


    // Misc
    TICKET
} 

public enum FurnitureID
{
    NONE = 0,
    BENCH,
    PLANTA,
}

public enum RecipeID
{
    NONE = 0,
    BENCH,
    PLANTA
}

public enum ItemType
{
    NONE = -1,
    MATERIAL,
    FOOD,
    FOODSPECIAL,
    COUNT
}

public enum FurnitureType
{
    NONE = -1,
    CHAIR,
    TABLE,
    STORAGE,
    NATURE,
    UTILITY,
    LIGHT,
    FUN,
    COUNT
}

public enum UIState
{
    SHOW,
    HIDE
}

public enum PalStatusID
{
    HUNGRY,
    SICK,
    HAPPY,
    SAD,
}

public enum UIID
{
    MAIN,
    DECORATION,
    POCKETS,
    DIALOG,
    CRAFTING,
    QUANTITY,
    EXPEDITIONS,
    MARKET
}

public enum MapID
{
    NONE = -1,
    FOREST,
    QUARRY,
    MESA,
    DLC_MINES,
    DLC_POPS,
    DLC_FOOD,
    DLC_TICKETS,
    COUNT
}

public enum LanguageID
{
    EN_US,          // English (US)
    PT_PT,          // Portuguese (Portugal)
    JA              // Japanese
}