﻿using System.Collections;
using UnityEngine;

public static class Constants
{
    public const int CAP_AGE = 7;
    public const int CAP_HUNGER = 100;
    public const int CAP_HAPPINESS = 50;
    public const int CAP_STAMINA = 100;

    // Minutes it takes to decrease stat
    public const int INTERVAL_HUNGER = 90;
    public const int INTERVAL_HAPPINESS = 120;

    public const int TICK_HAPPINESS = 2;
    public const int TICK_HUNGER = 5;

    public const int THRESHOLD_HUNGER = 40;

    public const int AGE_YOUNGIN = 1;
    public const int AGE_ADULT = 4;
    public const int AGE_OLDIE = 5;

    public const int INVENTORY_POCKETS = 4;
    public const int FURNITURE_TYPES = 7;

    public const string MENU_MAIN = "UIMainMenu";
    public const string MENU_DECO = "UIDecoration";
    public const string MENU_POCKETS = "UIPockets";

    public const int ITEM_STACK = 999;
    public const int STORAGE_ITEMS = 300;
    public const int STORAGE_ITEMS_UPGRADE = 1500;

    public const int STORAGE_FURNITURE = 500;
    public const int STORAGE_FURNITURE_UPGRADE = 1000;

    public const int MINUTES_IN_A_DAY = 1440;

    public const int POPS_MAX = 999999;

    // Scenes

    public const int SCENE_INIT = 0;
    public const int SCENE_GARDEN = 1;
}