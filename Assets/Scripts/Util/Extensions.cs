﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Random = UnityEngine.Random;

public static class Extensions
{

    #region Data

    public static FurnitureData GetData(this FurnitureID _id)
    {
        if (!GameDatabase.loaded)
        {
            throw new Exception($"FurnitureData {_id} doesn't exist. Database not initialized.");
        }

        var data = GameDatabase.database.dataFurniture.Where(x => x.id == _id).First();

        return data;
    }

    public static ItemData GetData(this ItemID _id)
    {
        if (!GameDatabase.loaded)
        {
            throw new Exception($"ItemData {_id} doesn't exist. Database not initialized.");
        }

        var data = GameDatabase.database.dataItem.Where(x => x.id == _id).First();

        return data;
    }

    public static MapData GetData(this MapID _id)
    {
        if (!GameDatabase.loaded)
        {
            throw new Exception($"MapData {_id} doesn't exist. Database not initialized.");
        }

        var data = GameDatabase.database.dataMaps.Where(x => x.id == _id).First();

        return data;
    }

    #endregion

    #region Sprites
    public static Sprite GetSprite(this ItemID _id)
    {
        switch (_id)
        {
            case ItemID.SPECIAL_RAREITEMS:
            case ItemID.SPECIAL_MOREIEMS:
            case ItemID.SPECIAL_SLOWSTATS:
            case ItemID.SPECIAL_STAMINA:
            case ItemID.SPECIAL_FULL:
                return Resources.Load<Sprite>("Items/special");
            default:
                return Resources.Load<Sprite>($"Items/{_id}");
        }
    }

    public static Sprite GetSprite(this FurnitureID _id)
    {
        return Resources.Load<Sprite>($"Items/Furniture/{_id}");
    }

    #endregion

    #region Strings

    public static string GetText(this ItemID id)
    {
        return Locale.GetText("itm-" + id.ToString());
    }

    public static string GetText(this MapID id)
    {
        return Locale.GetText("map-" + id.ToString());
    }

    public static string GetText(this FurnitureID id)
    {
        return Locale.GetText("ftr-" + id.ToString());
    }

    #endregion

}