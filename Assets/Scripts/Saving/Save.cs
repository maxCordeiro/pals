﻿using System.Collections;
using System;
using UnityEngine;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Linq;

[Serializable]
public class Save
{
    public int pops;

    public Pal pal;

    public List<ItemSlot>[] inventory;
    public List<FurnitureSlot>[] inventoryFurniture;

    public DateTime closedTime;

    public List<FurnitureSlot> gardenFurniture;

    public List<FurnitureID> unlockedRecipes;

    // Market
    public bool boughtSpecial;
    public bool[] boughtFurniture;
    public ItemID mktSpecial;
    public ItemID[] mktFood;
    public FurnitureID[] mktRecipes;
    public FurnitureID[] mktFurniture;

    public LanguageID selectedLanguage;

    public Save()
    {
        pops = 1000;

        inventory = new List<ItemSlot>[(int)ItemType.COUNT];
        inventoryFurniture = new List<FurnitureSlot>[(int)FurnitureType.COUNT];

        // Initialize inventory arrays
        for (int i = 0; i < inventoryFurniture.Length; i++)
        {
            inventoryFurniture[i] = new List<FurnitureSlot>();
        }

        for (int i = 0; i < inventory.Length; i++)
        {
            inventory[i] = new List<ItemSlot>();
        }

        gardenFurniture = new List<FurnitureSlot>();
        closedTime = DateTime.Now;
        unlockedRecipes = new List<FurnitureID>();
        selectedLanguage = LanguageID.EN_US;
    }

    public void GenerateMarketItems()
    {
        List<ItemData> eligibleSpecials = GameDatabase.database.dataItem.Where(x => x.function == ItemType.FOODSPECIAL).ToList();
        mktSpecial = eligibleSpecials[UnityEngine.Random.Range(0, eligibleSpecials.Count)].id;


        List<ItemData> eligibleFood = GameDatabase.database.dataItem.Where(x => x.function == ItemType.FOOD).ToList();
        mktFood = new ItemID[3];

        for (int i = 0;i < mktFood.Length; i++)
        {
            int index = UnityEngine.Random.Range(0, eligibleFood.Count);
            mktFood[i] = eligibleFood[index].id;
            if (eligibleFood.Count > mktFood.Length) eligibleFood.RemoveAt(index);
        }

        List<FurnitureData> eligibleRecipes = GameDatabase.database.dataFurniture;
        mktRecipes = new FurnitureID[4];

        for (int i = 0; i < mktRecipes.Length; i++)
        {
            int index = UnityEngine.Random.Range(0, eligibleRecipes.Count);
            mktRecipes[i] = eligibleRecipes[index].id;
            if (eligibleRecipes.Count > mktRecipes.Length) eligibleRecipes.RemoveAt(index);
        }

        List<FurnitureData> eligibleFurnture = GameDatabase.database.dataFurniture;
        mktFurniture= new FurnitureID[2];

        for (int i = 0; i < mktFurniture.Length; i++)
        {
            int index = UnityEngine.Random.Range(0, eligibleFurnture.Count);
            mktFurniture[i] = eligibleRecipes[index].id;
            if (eligibleFurnture.Count > mktFurniture.Length) eligibleFurnture.RemoveAt(index);
        }

        boughtSpecial = false;
        boughtFurniture = new bool[3]
        {
            false,
            false,
            false
        };
    }
}