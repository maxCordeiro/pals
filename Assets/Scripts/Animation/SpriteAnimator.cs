﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class SpriteAnimator : MonoBehaviour
{
    [System.Serializable]
    public class SpriteAnimation
    {
        public int index;
        public SpriteAnimationFrame[] frames;
        public float framesPerSec = 5;
        public bool loop = true;
        [Tooltip("Set SpriteRenderer to null after animation loop")]
        public bool clearRenderer;

        public float duration
        {
            get
            {
                return frames.Length * framesPerSec;
            }
            set
            {
                framesPerSec = value / frames.Length;
            }
        }

        public SpriteAnimation(int _index, SpriteAnimationFrame[] spr, bool lop)
        {
            index = _index; 
            frames = spr;
            loop = lop;
        }
    }

    [System.Serializable]
    public class SpriteAnimationFrame
    {
        public Sprite frameSprite;
        public float frameLength = -1;

        public SpriteAnimationFrame(Sprite _sprite, float _frameLength = -1)
        {
            frameSprite = _sprite;
            frameLength = _frameLength;
        }
    }

    public float globalFPS;

    public List<SpriteAnimation> animations = new List<SpriteAnimation>();

    [HideInInspector]
    public int currentFrame;

    public bool autoStart;

    [HideInInspector]
    public bool done
    {
        get { return currentFrame >= animations[currentlyPlaying].frames.Length; }
    }

    [HideInInspector]
    public bool playing
    {
        get { return _playing; }
    }

    SpriteRenderer spriteRenderer;
    public int currentlyPlaying;
    bool _playing;
    float secsPerFrame;
    float nextFrameTime;

    [HideInInspector]
    public int playCount = -1;
    [HideInInspector]
    public int currentPlay;


    [ContextMenu("Sort All Frames by Name")]
    void SortFramesByName()
    {
        foreach (SpriteAnimation anim in animations)
        {
            System.Array.Sort(anim.frames, (a, b) => a.frameSprite.name.CompareTo(b.frameSprite.name));
        }
        Debug.Log(gameObject.name + " animation frames have been sorted alphabetically.");
    }

    void Start()
    {
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        if (spriteRenderer == null)
        {
            Debug.Log(gameObject.name + ": Couldn't find SpriteRenderer");
        }

        if (animations.Count > 0 && autoStart) Play(0);
    }

    void Update()
    {
        if (!_playing || Time.time < nextFrameTime || spriteRenderer == null) return;
        currentFrame++;
        if (currentFrame >= animations[currentlyPlaying].frames.Length)
        {
            if (!animations[currentlyPlaying].loop || (playCount != -1 && currentPlay >= playCount))
            {
                _playing = false;
                if (animations[currentlyPlaying].clearRenderer) spriteRenderer.sprite = null;

                return;
            }
            currentFrame = 0;

            if (playCount != -1 && currentPlay < playCount)
            {
                currentPlay++;
            }
        }
        spriteRenderer.sprite = animations[currentlyPlaying].frames[currentFrame].frameSprite;
        nextFrameTime += (animations[currentlyPlaying].frames[currentFrame].frameLength <= 0 ? secsPerFrame : (1f / animations[currentlyPlaying].frames[currentFrame].frameLength));
    }

    public void Play(int index)
    {
        if (index < 0) return;
        SpriteAnimation anim = animations[index];

        currentlyPlaying = index;

        secsPerFrame = 1f / (animations[currentlyPlaying].frames[currentFrame].frameLength <= 0 ? globalFPS : animations[currentlyPlaying].frames[currentFrame].frameLength);//(globalFPS > 0 ? globalFPS : anim.framesPerSec);
        currentFrame = 0;
        _playing = true;
        nextFrameTime = Time.time;
    }

    public void Stop()
    {
        _playing = false;
    }

    public void Stop(bool reset)
    {
        currentFrame = 0;
        spriteRenderer.sprite = animations[currentlyPlaying].frames[0].frameSprite;
        _playing = false;
    }

    public void Resume()
    {
        _playing = true;
        nextFrameTime = Time.time + secsPerFrame;
    }
}