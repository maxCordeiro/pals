﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.IO;
using Newtonsoft.Json.UnityConverters.Math;


public class SaveController : MonoBehaviour
{
    public static Save save;

    private void OnEnable()
    {
        EventManager.onLoadFile += EventManager_onLoadFile;
        EventManager.onSaveFile += EventManager_onSaveFile;
        EventManager.onNewGame += EventManager_onNewGame;
    }
    private void OnDisable()
    {
        EventManager.onLoadFile -= EventManager_onLoadFile;
        EventManager.onSaveFile -= EventManager_onSaveFile;
    }

    private void EventManager_onLoadFile()
    {
        LoadGameFile();
    }

    private void EventManager_onSaveFile()
    {
        save.closedTime = DateTime.Now;
        SaveGameFile();

        Debug.Log($"Saved: {save.closedTime}");
    }

    private void EventManager_onNewGame()
    {
        NewGame();
    }

    #region Application
    private void OnApplicationFocus(bool focus)
    {
        if (!EventManager.inGarden) return;

        if (focus)
        {
            if (HasSaveFile)
            {
                EventManager.LoadGame();
            }
        }
        else
        {
            if (save != null)
            {
                EventManager.SaveGame();
            }
        }
    }

    private void OnApplicationPause(bool pause)
    {
        if (!EventManager.inGarden) return;

        if (!pause)
        {
            if (HasSaveFile)
            {
                EventManager.LoadGame();
            }
        }
        else
        {
            if (save != null)
            {
                EventManager.SaveGame();
            }
        }
    }

    private void OnApplicationQuit()
    {
        if (!EventManager.inGarden) return;

        if (save != null)
        {
            EventManager.SaveGame();
        }

    }

    #endregion

    #region Save Tools
    
    public static bool NewGame()
    {
        save = new Save();
        save.GenerateMarketItems();

        return SaveGameFile();
    }

    public static bool SaveGameFile()
    {
        var filePath = Path.Combine(Application.persistentDataPath, "game.sav");

        try
        {
            using (StreamWriter file = File.CreateText(filePath))
            {
                JsonSerializer serializer = new JsonSerializer()
                {
                    Formatting = Formatting.Indented,
                };

                serializer.Converters.Add(new Vector2Converter());
                serializer.Converters.Add(new StringEnumConverter());
                serializer.Serialize(file, save);
            }
        }
        catch (System.Exception)
        {
            return false;
        }

        return true;
    }

    public static bool LoadGameFile()
    {
        var filePath = Path.Combine(Application.persistentDataPath, "game.sav");

        if (!File.Exists(filePath))
        {
            return false;
        }

        try
        {
            using (StreamReader file = File.OpenText(filePath))
            {
                JsonSerializer serializer = new JsonSerializer();
                save = (Save)serializer.Deserialize(file, typeof(Save));
            }
        }
        catch (System.Exception)
        {
            return false;
        }

        return true;
    }

    public static bool HasSaveFile => File.Exists(Path.Combine(Application.persistentDataPath, "game.sav"));

    #endregion

}