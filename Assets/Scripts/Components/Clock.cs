﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class Clock : MonoBehaviour
{
    public int currentHour;
    public DateTime currentTime;
    public DateTime prevTime;
    public static double minutesPassed;
    public double timePassed;

    private void Start()
    {
        
    }

    private void Update()
    {
        if (!EventManager.inGarden) return;

        TickTime();
    }

    private void TickTime()
    {
        currentTime = DateTime.Now;

        if (prevTime.Minute != currentTime.Minute)
        {
            minutesPassed++;
        }

        if (minutesPassed > 0)
        {
            if (minutesPassed >= Constants.MINUTES_IN_A_DAY)
            {
                EventManager.DayPass((int)Math.Floor(minutesPassed / Constants.MINUTES_IN_A_DAY));
            }

            EventManager.MinutePass((int)minutesPassed);
        }

        prevTime = currentTime;
    }

    public void OnEnable()
    {
        EventManager.onLoadGame += EventManager_onLoadGame;
        EventManager.onNewGame += EventManager_onNewGame;
        EventManager.onMinutePass += EventManager_onMinutePass;
    }

    private void EventManager_onMinutePass(int obj)
    {
        minutesPassed -= obj;
    }

    private void OnDisable()
    {
        EventManager.onLoadGame -= EventManager_onLoadGame;
        EventManager.onNewGame -= EventManager_onNewGame;
    }

    private void EventManager_onLoadGame()
    {
        SetTimeNow();

        minutesPassed += (int)Math.Floor(TimeSpan.FromTicks(currentTime.Ticks - SaveController.save.closedTime.Ticks).TotalMinutes);

        // Called just to update the Pal's age if edited outside the game
        SaveController.save.pal.TickMinute(0);

        Debug.Log($"Loaded: Minutes Passed: {minutesPassed} ({SaveController.save.closedTime})");
    }

    private void EventManager_onNewGame()
    {
        SetTimeNow();
    }

    private void SetTimeNow()
    {
        currentTime = DateTime.Now;
        prevTime = currentTime;
    }
}