﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.InputSystem.EnhancedTouch;

public class GardenInitializer : MonoBehaviour
{
    public static bool initialized;

    private void OnEnable()
    {
        EventManager.onInitializeGarden += EventManager_onInitializeGarden;

        EnhancedTouchSupport.Enable();
        TouchSimulation.Enable();
    }

    private void OnDisable()
    {
        EventManager.onInitializeGarden -= EventManager_onInitializeGarden;

        EnhancedTouchSupport.Disable();
        TouchSimulation.Disable();
    }

    private void EventManager_onInitializeGarden()
    {
        EventManager.LoadDatabase();

        initialized = true;
    }
}