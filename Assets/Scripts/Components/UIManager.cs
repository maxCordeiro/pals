﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIManager : Singleton<UIManager>
{
    public EventSystem eventSys;
    public List<GameObject> menus;
    public GameObject menuParent;

    public static int menusOpen;
    public static int lastSelectedDialog;
    public static int lastQuantity;

    private Canvas gameCanvas;
    

    private void Awake()
    {
        GameObject canvasObj = GameObject.FindGameObjectWithTag("GameCanvas");
        
        if (canvasObj != null)
        {
            gameCanvas = canvasObj.GetComponent<Canvas>();
            gameCanvas.renderMode = RenderMode.ScreenSpaceCamera;
            gameCanvas.worldCamera = Camera.main;
            menuParent = gameCanvas.transform.GetChild(0).gameObject;

            for (int i = 0; i < menuParent.transform.childCount; i++)
            {
                menus.Add(menuParent.transform.GetChild(i).gameObject);
            }
        }

        eventSys = FindObjectOfType<EventSystem>();
    }

    private void Start()
    {
        // Disable container objects for menus after enabling
        // them to trigger Awake and OnEnable functions
        for (int i = 0; i < menus.Count; i++)
        {
            UIBase ui = menus[i].GetComponent<UIBase>();
            if (ui == null) continue;
            ui.EnableChildren(false);
        }
    }

    public static T GetMenu<T>()
    {
        return GetMenuObject<T>().GetComponent<T>();
    }

    public static GameObject GetMenuObject<T>()
    {
        return instance.menus.Find(x => x.GetComponent<T>() != null);
    }
}