﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Translation;

public class Locale : MonoBehaviour
{
    public static Translator translator;
    public static string[] langs;

    private void OnEnable()
    {
        EventManager.onInitializeGarden += EventManager_onInitializeGarden;
    }

    private void EventManager_onInitializeGarden()
    {
        try
        {
            translator = new Translator(ELangFormat.Csv, "Locale/Languages", ",");
            langs = translator.GetAvailableLanguages();

            translator.LoadDictionary(langs[0]);
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.Message);
            translator = null;
        }
    }

    private void OnDisable()
    {
        EventManager.onInitializeGarden -= EventManager_onInitializeGarden;
    }

    public static string GetText(string key)
    {
        string text = translator[key];
        return text.Length > 0 ? text : "*" + key;
    }

    public static string GetTextFormat(string key, params object[] args)
    {
        string text = translator[key];
        return text.Length > 0 ? string.Format(text, args) : "*" + key;
    }
}