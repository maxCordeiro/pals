﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.InputSystem.EnhancedTouch;
using Touch = UnityEngine.InputSystem.EnhancedTouch.Touch;

public class Decorator : MonoBehaviour
{
    public static bool isHoldingFurniture;
    public static bool isDraggingFurniture;

    public GameObject furniturePrefab;
    public GameObject furnitureParent;
    public SpriteRenderer spriteFurniturePreview;
    public int lastSelectedFurnitureIndex;
    public FurnitureType lastSelectedFurnitureType;

    public float rearrangeHoldCount;
    public bool rearrangeTimerOn;
    public bool isHoldingCounterFurniture;

    public BoxCollider2D colliderFurniturePreview;
    public BoxCollider2D colliderGarden;

    public LayerMask colliderFurnitureMask;
    public LayerMask colliderCounterMask;

    FurnitureEntity selectedFurniture;
    FurnitureEntity hitRearrangingEntity;


    private Vector3 touchPosRaw;
    private Vector3 touchPos;
    private Vector3 touchPosScreen;
    private Vector3 touchOrigin;
    private Vector3 rearrangeStartPos;
    private Vector3 rearrangeDistance;

    private Camera camMain;
    private UIDecoration uiDeco;

    private void Start()
    {
        colliderGarden = GameObject.FindGameObjectWithTag("GardenBounds").GetComponent<BoxCollider2D>();
        camMain = Camera.main;
        uiDeco = UIManager.GetMenu<UIDecoration>();
    }

    public void OnEnable()
    {
        EventManager.onLoadFurniture += EventManager_onLoadFurniture;
        EventManager.onStartDecoration += EventManager_onStartDecoration;
    }

    private void EventManager_onLoadFurniture(bool reset)
    {
        if (furnitureParent == null)
        {
            furnitureParent = GameObject.FindGameObjectWithTag("FurnitureParent");
        }

        if (reset)
        {
            foreach (FurnitureEntity entity in furnitureParent.transform)
            {
                Destroy(entity.gameObject);
            }
        }

        for (int i = 0; i < SaveController.save.gardenFurniture.Count; i++)
        {
            FurnitureSlot slot = SaveController.save.gardenFurniture[i];
            FurnitureEntity entity = Instantiate(furniturePrefab, slot.position, Quaternion.identity, furnitureParent.transform).GetComponent<FurnitureEntity>();
            entity.furniture = slot;
            entity.Initialize();
        }
    }

    private void OnDisable()
    {
        EventManager.onLoadFurniture -= EventManager_onLoadFurniture;
        EventManager.onStartDecoration -= EventManager_onStartDecoration;
    }

    private void Update()
    {
        if (!EventManager.inGarden) return;

        UpdateTouch();
        Decorate();
    }

    private void UpdateTouch()
    {
        if (!EventManager.inGarden) return;

        if (Touch.activeTouches.Count <= 0) return;

        touchPosRaw = Touch.activeTouches[0].screenPosition;
        touchPos = new Vector3(touchPosRaw.x, touchPosRaw.y, 0);
        touchPosScreen = camMain.ScreenToWorldPoint(touchPos);
    }

    public void Decorate()
    {
        if (isDraggingFurniture)
        {
            if (Touch.activeTouches.Count <= 0) return;

            spriteFurniturePreview.transform.position = new Vector3(touchPosScreen.x, touchPosScreen.y, 0);

            colliderFurniturePreview.transform.position = new Vector3(touchPosScreen.x, touchPosScreen.y, 0);

            if (!spriteFurniturePreview.gameObject.activeInHierarchy) spriteFurniturePreview.gameObject.SetActive(true);

            bool withinGarden = colliderGarden.bounds.Contains(colliderFurniturePreview.bounds.min) && colliderGarden.bounds.Contains(colliderFurniturePreview.bounds.max);

            RaycastHit2D furnitureHit = Physics2D.BoxCast(colliderFurniturePreview.bounds.center, colliderFurniturePreview.size, 0, Vector3.up, 0, colliderFurnitureMask);
            RaycastHit2D counterHit = Physics2D.BoxCast(colliderFurniturePreview.bounds.center, colliderFurniturePreview.size, 0, Vector3.up, 0, colliderCounterMask);

            Color red = new Color(1, 0, 0, 0.5f);
            Color clear = new Color(1, 1, 1, 0.5f);

            if (isHoldingCounterFurniture)
            {
                if (counterHit)
                {
                    spriteFurniturePreview.color = clear;
                }
                else
                {
                    if (furnitureHit || !withinGarden)
                    {
                        spriteFurniturePreview.color = red;
                    }
                    else
                    {
                        spriteFurniturePreview.color = clear;
                    }
                }
            }
            else
            {
                spriteFurniturePreview.color = (furnitureHit || !withinGarden) ? new Color(1, 0, 0, 0.5f) : new Color(1, 1, 1, 0.5f);
            }

            if (Touch.activeTouches[0].ended)
            {
                isDraggingFurniture = false;
                spriteFurniturePreview.gameObject.SetActive(false);

                if (!withinGarden)
                {
                    uiDeco.scroller.ReloadData();
                    uiDeco.ChangeState(UIState.SHOW);
                    return;
                }

                if (isHoldingCounterFurniture)
                {
                    if (furnitureHit && !counterHit)
                    {
                        uiDeco.scroller.ReloadData();
                        uiDeco.ChangeState(UIState.SHOW);
                        return;
                    }
                }
                else
                {
                    if (furnitureHit)
                    {
                        uiDeco.scroller.ReloadData();
                        uiDeco.ChangeState(UIState.SHOW);
                        return;
                    }
                }

                FurnitureEntity newEntity = Instantiate(furniturePrefab, new Vector3(touchPosScreen.x, touchPosScreen.y, 0), Quaternion.identity, furnitureParent.transform).GetComponent<FurnitureEntity>();
                FurnitureEntity counterEntity = null;

                if (isHoldingCounterFurniture && furnitureHit)
                {
                    counterEntity = furnitureHit.transform.GetComponent<FurnitureEntity>();
                }

                FurnitureSlot newSlot = SaveController.save.inventoryFurniture[(int)lastSelectedFurnitureType][lastSelectedFurnitureIndex];

                if (isHoldingCounterFurniture && furnitureHit)
                {
                    newSlot.isOnCounter = true;

                    // Assign and initialize FurnitureSlot to FurnitureEntity
                    newEntity.furniture = newSlot;
                    newEntity.Initialize();

                    // Add new FurnitureSlot to counter FurnitureSlot
                    //SaveSystem.instance.loadedGame.gardenFurniture[counterEntity.furniture.saveIndex].counterItems.Add(newSlot);
                }
                else
                {
                    // Assign and initialize FurnitureSlot to FurnitureEntity
                    newEntity.furniture = newSlot;
                    newEntity.Initialize();

                    newEntity.furniture.saveIndex = SaveController.save.gardenFurniture.Count;
                    newEntity.SyncPositions();

                    // Add to save
                    SaveController.save.gardenFurniture.Add(newEntity.furniture);
                }



                // Remove from the inventory
                SaveController.save.inventoryFurniture[(int)lastSelectedFurnitureType].RemoveAt(lastSelectedFurnitureIndex);


                StartCoroutine(FurniturePlacement(newEntity));

            }
        }
        else
        {
            if (Touch.activeTouches.Count <= 0) return;

            var hit = Physics2D.Raycast(touchPosScreen, Vector2.zero, 1f, colliderFurnitureMask);

            if (isHoldingFurniture)
            {
                Vector3 offsetPos = new Vector3(touchPosScreen.x + rearrangeDistance.x, touchPosScreen.y + rearrangeDistance.y, 0);

                spriteFurniturePreview.transform.position = offsetPos;

                colliderFurniturePreview.transform.position = offsetPos;

                bool withinGarden = colliderGarden.bounds.Contains(colliderFurniturePreview.bounds.min) && colliderGarden.bounds.Contains(colliderFurniturePreview.bounds.max);

                RaycastHit2D furnitureHit = Physics2D.BoxCast(colliderFurniturePreview.bounds.center, colliderFurniturePreview.size, 0, Vector3.up, 0, colliderFurnitureMask);

                spriteFurniturePreview.color = (furnitureHit || !withinGarden) ? new Color(1, 0, 0, 0.5f) : new Color(1, 1, 1, 0.5f);

                if (!spriteFurniturePreview.gameObject.activeInHierarchy) spriteFurniturePreview.gameObject.SetActive(true);

                if (Touch.activeTouches[0].ended)
                {
                    if (HUD.inDecoMenu)
                    {
                        uiDeco.ChangeState(UIState.SHOW);
                    }

                    if (!withinGarden || furnitureHit)
                    {
                        isHoldingFurniture = false;
                        spriteFurniturePreview.gameObject.SetActive(false);
                        return;
                    }

                    selectedFurniture.transform.position = offsetPos;
                    selectedFurniture.SyncPositions();
                    selectedFurniture.gameObject.SetActive(true);
                    spriteFurniturePreview.gameObject.SetActive(false);

                    StartCoroutine(FurniturePlacement(selectedFurniture, false));
                    isHoldingFurniture = false;
                }
            }
            else
            {
                if (hit && !UIManager.instance.eventSys.IsPointerOverGameObject())
                {
                    if (hitRearrangingEntity != hit.collider.gameObject.GetComponent<FurnitureEntity>())
                    {
                        hitRearrangingEntity = hit.collider.gameObject.GetComponent<FurnitureEntity>();
                    }

                    if (hitRearrangingEntity == null) return;

                    if (Touch.activeTouches[0].began)
                    {
                        rearrangeTimerOn = true;
                        rearrangeDistance = hitRearrangingEntity.transform.position - touchPosScreen;
                        EventManager.ToggleCameraDrag(false);
                    }

                    if (Touch.activeTouches[0].ended)
                    {
                        rearrangeTimerOn = false;
                        hitRearrangingEntity.isDragging = false;
                        EventManager.ToggleCameraDrag(true);
                    }

                    if (Touch.activeTouches[0].phase == UnityEngine.InputSystem.TouchPhase.Moved)
                    {
                        rearrangeHoldCount = 0;
                        rearrangeTimerOn = false;
                        EventManager.ToggleCameraDrag(true);
                        if (hitRearrangingEntity != null) hitRearrangingEntity.isDragging = false;
                    }

                    if (rearrangeTimerOn)
                    {
                        rearrangeHoldCount += Time.deltaTime;
                        if (rearrangeHoldCount >= 0.5f)
                        {
                            rearrangeHoldCount -= 0.5f;
                            rearrangeTimerOn = false;

                            spriteFurniturePreview.sprite = hitRearrangingEntity.furniture.id.GetSprite();

                            selectedFurniture = hitRearrangingEntity;
                            selectedFurniture.gameObject.SetActive(false);

                            FurnitureData data = hitRearrangingEntity.furniture.id.GetData();

                            colliderFurniturePreview.offset = data.boxOffset;
                            colliderFurniturePreview.size = data.boxSize;

                            if (HUD.inDecoMenu)
                            {
                                uiDeco.ChangeState(UIState.HIDE);
                            }
                            isHoldingFurniture = true;
                        }
                    }
                }
                else
                {
                    EventManager.ToggleCameraDrag(true);
                    rearrangeHoldCount = 0;
                    rearrangeTimerOn = false;
                    if (hitRearrangingEntity != null) hitRearrangingEntity.isDragging = false;
                }
            }
        }
    }

    IEnumerator FurniturePlacement(FurnitureEntity entity, bool reloadMenu = true)
    {
        LeanTween.scale(entity.gameObject, new Vector3(1.4f, 0.6f, 1f), 0.08f);
        LeanTween.scale(entity.gameObject, new Vector3(0.6f, 1.4f, 1f), 0.08f).setDelay(0.08f);
        LeanTween.scale(entity.gameObject, new Vector3(1f, 1f, 1f), 0.08f).setDelay(0.16f);

        yield return new WaitForSeconds(0.45f);

        if (reloadMenu)
        {
            uiDeco.scroller.ReloadData();
            uiDeco.ChangeState(UIState.SHOW);
        }

        yield break;
    }

    private void EventManager_onStartDecoration(int saveIndex, FurnitureID obj)
    {
        FurnitureData data = obj.GetData();

        colliderFurniturePreview.offset = data.boxOffset;
        colliderFurniturePreview.size = data.boxSize;

        spriteFurniturePreview.sprite = obj.GetSprite();
        lastSelectedFurnitureIndex = saveIndex;
        lastSelectedFurnitureType = data.type;
        isDraggingFurniture = true;
    }
}