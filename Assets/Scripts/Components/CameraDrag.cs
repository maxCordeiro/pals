﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.InputSystem.EnhancedTouch;
using Touch = UnityEngine.InputSystem.EnhancedTouch.Touch;

public class CameraDrag : MonoBehaviour
{
    public LayerMask cameraGroundMask;
    public BoxCollider2D cameraBounds;

    public static bool overObject;

    private Vector3 touchPosRaw;
    private Vector3 touchPos;
    private Vector3 touchPosScreen;
    private Vector3 touchOrigin;

    private bool canDrag;
    private Camera camMain;

    private float camWidth;
    private float camHeight;

    private void Start()
    {
        camMain = GetComponent<Camera>();
        camWidth = camMain.orthographicSize * camMain.aspect;
        camHeight = camMain.orthographicSize;

        cameraBounds = GameObject.Find("CameraBounds").GetComponent<BoxCollider2D>();
    }

    public void OnEnable()
    {
        EventManager.onToggleCameraDrag += EventManager_onToggleCameraDrag;
    }

    private void OnDisable()
    {
        EventManager.onToggleCameraDrag -= EventManager_onToggleCameraDrag;
    }

    private void EventManager_onToggleCameraDrag(bool active)
    {
        canDrag = active;
    }

    private void Update()
    {
        if (!canDrag) return;

        UpdateTouch();
        DragCamera();
    }

    private void UpdateTouch()
    {
        if (Touch.activeTouches.Count <= 0) return;

        touchPosRaw = Touch.activeTouches[0].screenPosition;
        touchPos = new Vector3(touchPosRaw.x, touchPosRaw.y, 0);
        touchPosScreen = camMain.ScreenToWorldPoint(touchPos);
    }

    private void DragCamera()
    {
        if (Touch.activeTouches.Count > 0)
        {
            // Check to see if the Touch is in its end state 
            // Needed to prevent dragging while in the Title Screen
            if (Touch.activeTouches[0].ended) return;

            Vector3 touchStart = Touch.activeTouches[0].startScreenPosition;

            if (!IsWithinBounds(touchStart)) return;

            var hit = Physics2D.Raycast(touchPosScreen, Vector2.zero, 1f, cameraGroundMask);

            Vector3 diff = touchPosScreen - camMain.transform.position;

            if (hit)
            {
                if (Touch.activeTouches[0].began)
                {
                    touchOrigin = touchPosScreen;

                    if (UIManager.instance.eventSys != null && UIManager.instance.eventSys.IsPointerOverGameObject())
                    {
                        if (hit.collider.gameObject.layer != 7)
                        {
                            overObject = true;
                        }
                    }
                }

                if (overObject) return;

                Vector3 camPos = new Vector3(touchOrigin.x - diff.x, touchOrigin.y - diff.y, -10);

                camMain.transform.position = ClampToBounds(camPos);
            }
        }
        else
        {
            overObject = false;
        }
    }

    public bool IsWithinBounds(Vector3 _touchStart)
    {
        return (_touchStart.x > 0 && _touchStart.x < Screen.width && _touchStart.y < Screen.height && _touchStart.y > 0);
    }

    public Vector3 ClampToBounds(Vector3 _camPos)
    {
        float _x = Mathf.Clamp(_camPos.x, -cameraBounds.bounds.extents.x + camWidth, cameraBounds.bounds.extents.x - camWidth);
        float _y = Mathf.Clamp(_camPos.y, -cameraBounds.bounds.extents.y + camHeight, cameraBounds.bounds.extents.y - camHeight);

        return new Vector3(_x, _y, _camPos.z);
    }

}