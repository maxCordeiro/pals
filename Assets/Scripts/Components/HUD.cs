﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    public static bool inMainMenu;
    public static bool inDecoMenu;

    private Canvas hudCanvas;

    public GameObject statusParent;
    public GameObject buttonsParent;

    public SwitchButton buttonMenu;
    public SwitchButton buttonDeco;

    public Image[] imageStatusBar;

    public bool MenuOpen => inMainMenu || inDecoMenu;
    public static bool ShouldShowStatus => SaveController.save.pal.ageID != PalAges.EGG && !SaveController.save.pal.isOnExpedition;

    private UIMainMenu menuMain;
    private UIDecoration menuDeco;

    private void Start()
    {
        hudCanvas = transform.GetChild(0).GetComponent<Canvas>();
        hudCanvas.renderMode = RenderMode.ScreenSpaceCamera;
        hudCanvas.worldCamera = Camera.main;
    }

    private void OnEnable()
    {
        EventManager.onLoadGame += EventManager_onLoadGame;

        EventManager.onToggleHUDButtons += EventManager_onToggleHUDButtons;
        EventManager.onToggleHUDStatus += EventManager_onToggleHUDStatus;

        EventManager.onRefreshHUD += EventManager_onRefreshHUD;

        buttonMenu.onClick += Click_Main;
        buttonDeco.onClick += Click_Deco;

        menuMain = UIManager.GetMenu<UIMainMenu>();

        menuMain.onShow += MenuMain_onShow;
        menuMain.onHide += MenuMain_onHide;

        menuDeco = UIManager.GetMenu<UIDecoration>();

        menuDeco.onShow += MenuDeco_onShow;
        menuDeco.onHide += MenuDeco_onHide;
    }

    private void OnDisable()
    {
        EventManager.onLoadGame -= EventManager_onLoadGame;

        EventManager.onToggleHUDButtons -= EventManager_onToggleHUDButtons;
        EventManager.onToggleHUDStatus -= EventManager_onToggleHUDStatus;

        EventManager.onRefreshHUD -= EventManager_onRefreshHUD;

        buttonMenu.onClick -= Click_Main;
        buttonDeco.onClick -= Click_Deco;

        menuMain.onShow -= MenuMain_onShow;
        menuMain.onHide -= MenuMain_onHide;

        menuDeco.onShow -= MenuDeco_onShow;
        menuDeco.onHide -= MenuDeco_onHide;
    }

    private void EventManager_onLoadGame()
    {
        EventManager.ToggleHUDStatus(!MenuOpen && ShouldShowStatus);
        EventManager.RefreshHUD();
    }
    private void EventManager_onToggleHUDStatus(bool isToggled)
    {
        statusParent.SetActive(isToggled);
    }

    private void EventManager_onToggleHUDButtons(bool toggleDeco, bool toggleMain)
    {
        buttonDeco.gameObject.SetActive(toggleDeco);
        buttonMenu.gameObject.SetActive(toggleMain);
    }

    private void EventManager_onRefreshHUD()
    {
        float hungerAmt = (float)SaveController.save.pal.hunger / (float)Constants.CAP_HUNGER;
        float staminaAmt = (float)SaveController.save.pal.stamina / (float)Constants.CAP_STAMINA;
        float happyAmt = (float)SaveController.save.pal.happiness / (float)Constants.CAP_HAPPINESS;

        imageStatusBar[0].fillAmount = hungerAmt;
        imageStatusBar[1].fillAmount = staminaAmt;
        imageStatusBar[2].fillAmount = happyAmt;
    }

    private void Click_Deco()
    {
        if (menuDeco.isAnimating) return;
        
        buttonDeco.ToggleInteractable(false, false);

        if (inDecoMenu)
        {
            EventManager.ToggleHUDButtons(true, true);

            menuDeco.ChangeState(UIState.HIDE);
        } else
        {
            EventManager.ToggleHUDStatus(false);
            EventManager.ToggleHUDButtons(true, false);
            inDecoMenu = true;

            menuDeco.transform.SetAsLastSibling();
            menuDeco.ChangeState(UIState.SHOW);
        }
    }

    private void MenuDeco_onHide()
    {
        if (Decorator.isHoldingFurniture || Decorator.isDraggingFurniture) return;

        EventManager.ToggleHUDStatus(ShouldShowStatus);
        inDecoMenu = false;
        buttonDeco.ToggleInteractable(true, true);
        EventManager.ReturnToGarden();
    }

    private void MenuDeco_onShow()
    {
        buttonDeco.ToggleInteractable(true, true);
    }

    private void Click_Main()
    {
        if (menuMain.isAnimating) return;
        
        buttonMenu.ToggleInteractable(false, false);

        if (inMainMenu)
        {
            EventManager.ToggleHUDButtons(true, true);

            EventManager.ToggleHUDStatus(ShouldShowStatus);
            menuMain.ChangeState(UIState.HIDE);
        } else
        {
            EventManager.ToggleHUDButtons(false, true);

            inMainMenu = true;
            menuMain.transform.SetAsLastSibling();
            menuMain.ChangeState(UIState.SHOW);
        }
    }

    private void MenuMain_onHide()
    {
        if (menuMain.referrerOpen)
        {
            menuMain.referrerOpen = false;
            return;
        }

        EventManager.RefreshHUD();
        EventManager.ToggleHUDStatus(ShouldShowStatus);
        EventManager.RefreshPalController();
        
        inMainMenu = false;
        buttonMenu.ToggleInteractable(true);
        

        EventManager.ReturnToGarden();
    }

    private void MenuMain_onShow()
    {
        buttonMenu.ToggleInteractable(true);
        EventManager.ToggleHUDStatus(false);        
    }
}