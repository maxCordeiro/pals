﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Newtonsoft.Json;
using System.IO;

public class GameDatabase : MonoBehaviour
{
    public static Database database;
    public static bool loaded;

    private void OnEnable()
    {
        EventManager.onLoadDatabase += EventManager_onLoadDatabase;
    }

    private void OnDisable()
    {
        EventManager.onLoadDatabase -= EventManager_onLoadDatabase;
    }

    private void EventManager_onLoadDatabase()
    {
        if (loaded) return;

        database = new Database();
        TextAsset dbJSON = Resources.Load<TextAsset>("db");
        JsonSerializer serializer = new JsonSerializer();
        database = (Database)serializer.Deserialize(new StringReader(dbJSON.text), typeof(Database));
        
        loaded = true;
    }
}