using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct ItemData
{
    public ItemID id;
    public ItemType function;
    public int sellPrice;
}