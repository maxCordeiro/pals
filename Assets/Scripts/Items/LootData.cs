﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

[Serializable]
public struct LootData
{
    public ItemID id;

    public float chance;

    public int minQuantity;
    public int maxQuantity;
}
