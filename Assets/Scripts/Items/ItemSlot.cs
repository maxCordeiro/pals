using System;
using UnityEngine;

[Serializable]
public class ItemSlot {
    public ItemID id;
    public int quantity;
}