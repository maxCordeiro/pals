﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Random = UnityEngine.Random;

public static class GameHelper
{
    #region Items

    // Returns
    // 1 = Added new item
    // 2 = Added quantity
    // 3 = Quantity would exceed max stack, can't add
    // 4 = Adding would surpass item storage, can't add
    public static int AddItem(ItemSlot _item)
    {
        Debug.Log("Adding item.");
        ItemData data = _item.id.GetData();
        int saveIndex = SaveController.save.inventory[(int)data.function].FindIndex(x => x.id == _item.id);

        if (GetItemStorage() + _item.quantity > Constants.STORAGE_ITEMS)
        {
            Debug.Log("Will exceed item storage.");
            return 4;
        }

        if (saveIndex == -1) // If there's no existing item in the inventory
        {
            SaveController.save.inventory[(int)data.function].Add(_item);

            Debug.Log("Success!");
            return 1;
        }
        else
        {
            int totalQuantity = _item.quantity + SaveController.save.inventory[(int)data.function][saveIndex].quantity;

            if (totalQuantity < Constants.ITEM_STACK)
            {
                SaveController.save.inventory[(int)data.function][saveIndex].quantity += _item.quantity;
                Debug.Log("Success! Adding to existing item stack.");
                return 2;
            }
            else
            {
                Debug.Log("Will exceed stack quantity.");
                return 3;
            }
        }
    }

    public static int AddItem(ItemID _id, int _quantity = 1)
    {
        return AddItem(new ItemSlot()
        {
            id = _id,
            quantity = _quantity
        });
    }

    public static int GetItemStorage()
    {
        int totalAmount = 0;

        for (int i = 0; i < (int)ItemType.COUNT; i++)
        {
            for (int j = 0; j < SaveController.save.inventory[i].Count; j++)
            {
                totalAmount += SaveController.save.inventory[i][j].quantity;
            }
        }

        return totalAmount;
    }

    public static bool HasItem(ItemID _id)
    {
        bool hasItem = false;

        for (int i = 0; i < SaveController.save.inventory.Length; i++)
        {
            hasItem = SaveController.save.inventory[i].Any(x => x.id == _id);
        }

        return hasItem;
    }

    public static int GetItemPocketIndex(ItemID _id)
    {
        return (int)_id.GetData().function;
    }

    public static int GetItemInventoryIndex(ItemID _id, int pocketIndex = -1)
    {
        return SaveController.save.inventory[pocketIndex == -1 ? GetItemPocketIndex(_id) : pocketIndex].FindIndex(x => x.id == _id);
    }

    public static void ConsumeItem(ItemID _id, int _quantity)
    {
        int pocketIndex = GetItemPocketIndex(_id);
        int inventoryIndex = GetItemInventoryIndex(_id);

        SaveController.save.inventory[pocketIndex][inventoryIndex].quantity -= _quantity;

        if (SaveController.save.inventory[pocketIndex][inventoryIndex].quantity <= 0)
        {
            SaveController.save.inventory[pocketIndex].RemoveAt(inventoryIndex);
        }
    }

    #endregion

    #region Furniture

    public static bool AddFurniture(FurnitureID _id)
    {
        return AddFurniture(new FurnitureSlot()
        {
            id = _id
        });
    }

    public static bool AddFurniture(FurnitureSlot _furniture)
    {
        FurnitureData data = _furniture.id.GetData();

        if (GetFurnitureStorage() + 1 > Constants.STORAGE_FURNITURE)
        {
            return false;
        }

        SaveController.save.inventoryFurniture[(int)data.type].Add(_furniture);

        return true;
    }

    public static int GetFurnitureStorage()
    {
        int totalAmount = 0;

        for (int i = 0; i < (int)FurnitureType.COUNT; i++)
        {
            for (int j = 0; j < SaveController.save.inventoryFurniture[i].Count; j++)
            {
                // FurnitureSlot does not have quantity
                totalAmount++;
            }
        }

        return totalAmount;
    }

    public static bool Craft(FurnitureID _id)
    {
        if (CanCraft(_id))
        {
            return AddFurniture(_id);
        }

        return false;
    }

    public static bool CanCraft(FurnitureID _id)
    {
        FurnitureData data = _id.GetData();

        int itemsSaveHas = 0;

        for (int i = 0; i < data.requiredMaterials.Count; i++)
        {
            ItemData itemData = data.requiredMaterials[i].id.GetData();

            if (HasItem(itemData.id))
            {
                if (SaveController.save.inventory[(int)itemData.function][(int)itemData.id].quantity >= data.requiredMaterials[i].quantity)
                {
                    itemsSaveHas++;
                }
            }
        }

        return itemsSaveHas == data.requiredMaterials.Count;

    }

    public static int GetMaxCraftableFurniture(FurnitureID _id)
    {
        FurnitureData data = _id.GetData();
        int prevMax = 0;

        for (int i = 0; i < data.requiredMaterials.Count; i++)
        {
            ItemSlot material = data.requiredMaterials[i];
            ItemData itemData = material.id.GetData();

            int hasQuantity = SaveController.save.inventory[(int)itemData.function][GetItemPocketIndex(itemData.id)].quantity;

            if (hasQuantity >= material.quantity)
            {
                int currMax = Mathf.FloorToInt(hasQuantity / material.quantity);

                if (prevMax == 0 || prevMax > currMax)
                {
                    prevMax = currMax;
                }
            } else
            {
                return 0;
            }
        }

        return prevMax;
    }

    #endregion

    #region Recipes

    // Returns false if the recipe is already unlocked
    public static bool UnlockRecipe (FurnitureID _id)
    {
        if (SaveController.save.unlockedRecipes.Contains(_id))
        {
            return false;
        }

        SaveController.save.unlockedRecipes.Add(_id);
        return true;
    }

    #endregion

    #region Maps

    public static List<ItemSlot> GetExpeditionLoot(MapID _id)
    {
        MapData _mapData = _id.GetData();
        List<ItemSlot> returnLoot = new List<ItemSlot>();
        float drawn = Random.Range(0f, 1f);

        foreach (LootData loot in _mapData.loot)
        {
            ItemSlot item = new ItemSlot();

            if (drawn <= loot.chance)
            {
                item.id = loot.id;
                item.quantity = Random.Range(loot.minQuantity, loot.maxQuantity);
                returnLoot.Add(item);
            }
        }

        // Add food items here

        // Add ticket item here

        return returnLoot;
    }

    public static int GetExpeditionPops(MapID _id, List<ItemSlot> _loot)
    {
        return _loot.Count * (_id == MapID.DLC_POPS ? 200 : 100);
    }

    #endregion

    #region Pops

    public static void AddPops(int _amt)
    {
        SaveController.save.pops = Mathf.Clamp(SaveController.save.pops + _amt, 0, Constants.POPS_MAX);
    }

    #endregion
}