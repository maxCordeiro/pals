using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using EnhancedUI.EnhancedScroller;

public class UIDialog : UIBase
{
    [Header("Components")]
    public DialogButton[] choiceButtons;
    public SuperTextMesh textMessage;

    int buttonCount;

    public override void UIInit()
    {
        UIManager.lastSelectedDialog = -1;
        buttonCount = 0;
    }

    public void SetMessage(string _text)
    {
        textMessage.text = _text;
    }

    public void AddChoice(int _id, string _text)
    {
        if (_id > choiceButtons.Length) Debug.LogError("UIDialog Button ID " + _id + " doesn't exist");

        choiceButtons[_id].id = _id;
        choiceButtons[_id].buttonText.text = _text;

        buttonCount++;
    }

    public void SetButtons()
    {
        for (int i = 0; i < choiceButtons.Length; i++)
        {
            if (i > buttonCount - 1)
            {
                choiceButtons[i].gameObject.SetActive(false);
            } else
            {
                choiceButtons[i].gameObject.SetActive(true);
            }
        }
    }
}
