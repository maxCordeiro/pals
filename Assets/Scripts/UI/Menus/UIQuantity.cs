using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIQuantity : UIBase
{
    [Header("Components")]
    public SimpleButton buttonConfirm;
    public SuperTextMesh textMessage;
    public SimpleButton[] buttonQuantity;
    public SuperTextMesh textQuantity;

    [Header("Logic")]
    public int quantityMax, quantityMin;
    int quantity;

    // Start is called before the first frame update
    void Start()
    {
    }

    public override void UIInit()
    {

    }

    private void OnEnable()
    {
        buttonConfirm.onClick += Click_OKQuantity;

        buttonQuantity[0].onClick += Click_Decrease;
        buttonQuantity[1].onClick += Click_Increase;

        onShow += UIQuantity_onShow;
        onPreShow += UIQuantity_onPreShow;

        onPreHide += SetQuantity;
    }

    private void UIQuantity_onPreShow()
    {
        quantity = 0;
        textQuantity.text = quantity.ToString();
    }

    private void UIQuantity_onShow()
    {
        UIManager.lastQuantity = quantity;
    }

    private void OnDisable()
    {
        buttonConfirm.onClick -= Click_OKQuantity;

        buttonQuantity[0].onClick -= Click_Decrease;
        buttonQuantity[1].onClick -= Click_Increase;

        onShow -= UIQuantity_onShow;
        onPreShow -= UIQuantity_onPreShow;

        onPreHide -= SetQuantity;
    }

    public void SetMinMax(int min, int max)
    {
        quantityMin = min;
        quantityMax = max;
    }

    public void SetOKText(string text)
    {
        buttonConfirm.transform.GetChild(0).GetComponent<SuperTextMesh>().text = text;
    }

    public void SetMessageText(string text)
    {
        textMessage.text = text;
    }

    void Click_OKQuantity()
    {
        ChangeState(UIState.HIDE);
    }

    void SetQuantity()
    {
        UIManager.lastQuantity = quantity;
    }

    void Click_Decrease()
    {
        quantity = Mathf.Clamp(--quantity, quantityMin, quantityMax);
        textQuantity.text = quantity.ToString();
    }

    void Click_Increase()
    {
        quantity = Mathf.Clamp(++quantity, quantityMin, quantityMax);
        textQuantity.text = quantity.ToString();
    }
}
