using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EnhancedUI;
using EnhancedUI.EnhancedScroller;

public class UIDecoration : UIBase, IEnhancedScrollerDelegate
{
    [Header("Logic")]
    public FurnitureType selectedFurnitureType;

    [Header("Scroller")]
    public EnhancedScroller scroller;
    public EnhancedScrollerCellView cellViewPrefab;

    public float cellViewSize;

    [Header("UI")]
    public GameObject decoButtonsParent;
    public ToggleButton[] decoButtons;

    public Image imageFurniturePreview;

    public override void UIInit()
    {
        scroller.ReloadData();
    }

    // Start is called before the first frame update
    void Start()
    {
        selectedFurnitureType = FurnitureType.CHAIR;

        decoButtons = new ToggleButton[decoButtonsParent.transform.childCount];

        for (int i = 0; i < decoButtons.Length; i++)
        {
            int ii = i;
            decoButtons[i] = decoButtonsParent.transform.GetChild(i).GetComponent<ToggleButton>();
            decoButtons[i].onToggle += () =>
            {
                selectedFurnitureType = (FurnitureType)ii;
                scroller.ReloadData();
            };
        }

        scroller.Delegate = this;

        decoButtons[0].isToggled = true;
        decoButtons[0].Refresh();
    }


    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        return SaveController.save.inventoryFurniture[(int)selectedFurnitureType].Count;
    }

    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        return cellViewSize;
    }

    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        CellViewFurniture cellView = scroller.GetCellView(cellViewPrefab) as CellViewFurniture;

        cellView.name = "Cell " + dataIndex.ToString();
        cellView.Refresh(SaveController.save.inventoryFurniture[(int)selectedFurnitureType][dataIndex]);

        return cellView;
    }

    public void ToggleContainer(int active)
    {
        transform.GetChild(0).gameObject.SetActive(active == 0? false : true);
    }
}
