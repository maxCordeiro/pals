﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;


public class UIMainMenu : UIBase
{
    [Header("UI")]
    public SimpleButton[] menuButtons;

    public bool referrerOpen;

    private UICrafting uiCrafting;
    private UIExpeditions uiExp;
    private UIMarket uiMarket;
    private UIPockets uiPocket;

    private void Start()
    {
        uiCrafting = UIManager.GetMenu<UICrafting>();
        uiExp = UIManager.GetMenu<UIExpeditions>();
        uiMarket = UIManager.GetMenu<UIMarket>();
        uiPocket = UIManager.GetMenu<UIPockets>();
    }

    private void OnEnable()
    {
        for (int i = 0; i < menuButtons.Length; i++)
        {
            int ii = i;

            menuButtons[i].onClick += () =>
            {
                Hide_Open(ii);
            };
        }

        onShow += ToggleMenuButton;
    }

    private void OnDisable()
    {
        for (int i = 0; i < menuButtons.Length; i++)
        {
            menuButtons[i].RemoveAllListeners();
        }

        onShow -= ToggleMenuButton;
    }

    public override void UIInit()
    {
        bool isEgg = SaveController.save.pal.ageID == PalAges.EGG;

        for (int i = 0; i < menuButtons.Length; i++)
        {
            if (i < menuButtons.Length - 1)
            {
                menuButtons[i].ToggleInteractable(!isEgg, true);
            }
        }
    }

    public void Hide_Open(int index)
    {
        EventManager.ToggleHUDButtons(false, false);
        ResetHideEvents();

        referrerOpen = true;

        switch (index)
        {
            case 0:
                onHide += Open_Expedition;
                break;
            case 1:
                onHide += Open_Pockets;
                break;
            case 2:
                onHide += Open_Crafting;
                break;
            case 3:
                onHide += Open_Market;
                break;
            case 4:
                onHide += Open_Settings;
                break;
            default:
                break;
        }

        onHide += ResetHideEvents;

        ChangeState(UIState.HIDE);
    }

    public void Open_Expedition()
    {
        uiExp.transform.SetAsLastSibling();
        if (uiExp.isAnimating) return;

        uiExp.referrer = this;
        uiExp.ChangeState(UIState.SHOW);
    }

    public void Open_Gallery()
    {

    }

    public void Open_Pockets()
    {
        uiPocket.transform.SetAsLastSibling();
        if (uiPocket.isAnimating) return;

        uiPocket.referrer = this;
        uiPocket.ChangeState(UIState.SHOW);
    }

    public void Open_Crafting()
    {
        uiCrafting.transform.SetAsLastSibling();
        if (uiCrafting.isAnimating) return;

        uiCrafting.referrer = this;
        uiCrafting.ChangeState(UIState.SHOW);
    }

    public void Open_Play()
    {

    }

    public void Open_Market()
    {
        uiMarket.transform.SetAsLastSibling();
        if (uiMarket.isAnimating) return;

        uiMarket.referrer = this;
        uiMarket.ChangeState(UIState.SHOW);
    }

    public void Open_Settings()
    {

    }

    public void ResetHideEvents()
    {
        onHide -= Open_Expedition;
        onHide -= Open_Gallery;
        onHide -= Open_Pockets;
        onHide -= Open_Crafting;
        onHide -= Open_Play;
        onHide -= Open_Market;
        onHide -= Open_Settings;

        onHide -= ToggleMenuButton;
        onHide -= ResetHideEvents;
    }

    public void ToggleMenuButton()
    {
        EventManager.ToggleHUDButtons(false, true);
    }
}