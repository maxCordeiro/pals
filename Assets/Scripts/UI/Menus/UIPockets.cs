using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EnhancedUI.EnhancedScroller;

public class UIPockets : UIBase, IEnhancedScrollerDelegate
{
    [Header("References")]
    public GameObject buttonPocket;
    public GameObject buttonPocketParent;

    [Header("Components")]
    public EnhancedScroller scroller;
    public EnhancedScrollerCellView cellViewPrefab;
    public SimpleButton buttonClose;

    public SuperTextMesh textStorage;

    [Header("Sprites")]
    public Sprite[] iconsPocket;

    public ToggleButton[] buttonPocketToggle;

    [Header("Logic")]
    public ItemType selectedItemType;
    public int numberOfCellsPerRow;

    // Start is called before the first frame update
    void Start()
    {
        scroller.Delegate = this;

        selectedItemType = ItemType.MATERIAL;

        buttonPocketToggle = new ToggleButton[(int)ItemType.COUNT];

        for (int i = 0; i < (int)ItemType.COUNT; i++)
        {
            int ii = i;

            ToggleButton button = Instantiate(buttonPocket, buttonPocketParent.transform).GetComponent<ToggleButton>();
            button.spriteIconNormal = iconsPocket[i];
            button.spriteIconSelect = iconsPocket[i];
            button.Refresh();

            button.onToggle += () =>
            {
                selectedItemType = (ItemType)ii;
                UpdateStorageText();
                scroller.ReloadData();
            };

            buttonPocketToggle[i] = button;
        }

        foreach (ToggleButton toggle in buttonPocketToggle)
        {
            toggle.toggleParent = toggle.transform.parent.gameObject;
            toggle.DefineToggleButtons();
        }

        buttonPocketToggle[0].isToggled = true;
        buttonPocketToggle[0].Refresh();

    }

    public override void UIInit()
    {
        scroller.ReloadData();
        UpdateStorageText();
    }

    private void OnEnable()
    {
        buttonClose.onClick += Click_Hide;
    }

    private void OnDisable()
    {
        buttonClose.onClick -= Click_Hide;
    }

    private void Click_Hide()
    {
        ChangeState(UIState.HIDE);
    }

    public void UpdateStorageText()
    {
        textStorage.text = $"{GameHelper.GetItemStorage()}/{Constants.STORAGE_ITEMS}";
    }


    public void DialogCallback()
    {
        Freeze(false);

        switch (UIManager.lastSelectedDialog)
        {
            case 0:
                Debug.Log("0");
                break;
            case 1:
                Debug.Log("1");
                break;
            default:
                break;
        }
    }

    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        return SaveController.save.inventory[(int)selectedItemType].Count;
    }

    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        return 188f;
    }

    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        CellViewItem cellView = scroller.GetCellView(cellViewPrefab) as CellViewItem;

        for (int i = 0; i < cellView.rowCellViews.Length; i++)
        {
            cellView.rowCellViews[i].uiPockets = this;
        }

        cellView.name = "Cell " + (dataIndex * numberOfCellsPerRow).ToString() + " to " + ((dataIndex * numberOfCellsPerRow) + numberOfCellsPerRow - 1).ToString();
        cellView.Refresh((int)selectedItemType, dataIndex * numberOfCellsPerRow);

        return cellView;
    }
}
