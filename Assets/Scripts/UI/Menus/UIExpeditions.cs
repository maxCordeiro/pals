﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnhancedUI.EnhancedScroller;
using UnityEngine;
using UnityEngine.UI;

public class UIExpeditions : UIBase, IEnhancedScrollerDelegate
{
    [Header("Scroller")]
    public EnhancedScrollerCellView cellViewPrefab;

    [Header("Components")]
    public EnhancedScroller scrollerMaps;
    public SimpleButton buttonClose;
    public SimpleButton buttonGo;

    public GameObject scrollPanel;
    public GameObject onExpeditionPanel;

    public SuperTextMesh textOnExpedition;
    public Image imagePalMarker;
    public Image imageProgress;

    public Image imageHome;
    public Image imageGoal;

    [Header("Logic")]
    public static int selectedMap;

    private UIDialog dialog;
    public Sprite[] expeditionIcons;
    
    public void Start()
    {
        scrollerMaps.Delegate = this;
        dialog = UIManager.GetMenu<UIDialog>();
    }

    private void OnEnable()
    {
        EventManager.onMinutePass += EventManager_onMinutePass;

        buttonClose.onClick += Click_Hide;
        buttonGo.onClick += Click_Go;
    }

    private void EventManager_onMinutePass(int obj)
    {
        if (!SaveController.save.pal.isOnExpedition) return;

        UpdateExpeditionText();
    }

    private void OnDisable()
    {
        buttonClose.onClick -= Click_Hide;
        buttonGo.onClick -= Click_Go;

        EventManager.onMinutePass -= EventManager_onMinutePass;
    }

    public void Click_Hide()
    {
        ChangeState(UIState.HIDE);
    }

    public void Click_Go()
    {
        Freeze();

        MapData data = ((MapID)selectedMap).GetData();
        dialog.transform.SetAsLastSibling();

        dialog.SetMessage(Locale.GetTextFormat("explore-go", SaveController.save.pal.palName, (MapID)selectedMap, data.timeToComplete * 60));
        dialog.AddChoice(0, Locale.GetText("explore-yes"));
        dialog.AddChoice(1, Locale.GetText("explore-no"));
        dialog.SetButtons();
        dialog.onHide -= Unfreeze;
        dialog.onHide += Unfreeze;
        dialog.onHide -= CallBack_Expedition;
        dialog.onHide += CallBack_Expedition;
        dialog.ChangeState(UIState.SHOW);
    }
    public void Unfreeze()
    {
        dialog.transform.SetAsLastSibling();
        dialog.onHide -= Unfreeze;
        dialog.onHide -= CallBack_Expedition;

        Freeze(false);
        transform.SetAsLastSibling();
    }

    public void CallBack_Expedition()
    {
        switch (UIManager.lastSelectedDialog)
        {
            case 0:
                SaveController.save.pal.isOnExpedition = true;
                SaveController.save.pal.expeditionTimer = ((MapID)selectedMap).GetData().timeToComplete * 60;
                SaveController.save.pal.expeditionMap = (MapID)selectedMap;

                TogglePanel();
                break;
            default:
                break;
        }
    }

    public override void UIInit()
    {
        selectedMap = 0;
        
        TogglePanel();
    }

    public void TogglePanel()
    {
        bool isOnExpedition = SaveController.save.pal.isOnExpedition;

        scrollPanel.SetActive(!isOnExpedition);
        onExpeditionPanel.SetActive(isOnExpedition);

        if (!isOnExpedition)
        {
            scrollerMaps.ReloadData();
            buttonGo.ToggleInteractable(true);
        } else
        {
            UpdateExpeditionText();
        }
    }

    public void UpdateExpeditionText()
    {
        if (!SaveController.save.pal.isOnExpedition) return;

        float percent = 1f - (SaveController.save.pal.expeditionTimer / (SaveController.save.pal.expeditionMap.GetData().timeToComplete * 60f));

        imagePalMarker.transform.position = Vector3.Lerp(imageHome.transform.position, imageGoal.transform.position, percent);

        string text = Locale.GetTextFormat("explore-wait", SaveController.save.pal.palName, SaveController.save.pal.expeditionTimer);

        textOnExpedition.text = text;
    }

    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        CellViewMap cellView = scroller.GetCellView(cellViewPrefab) as CellViewMap;

        cellView.uiExpeditions = this;
        cellView.name = "Map " + ((MapID)dataIndex).ToString();
        cellView.Refresh(dataIndex);

        return cellView;
    }

    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        return 316f;
    }

    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        // TODO: Varying number depending on DLC
        return 2;// GameDatabase.database.dataMaps.Count;
    }

}