﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using EnhancedUI.EnhancedScroller;

public class UICrafting : UIBase, IEnhancedScrollerDelegate
{
    [Header("Scroller")]
    public EnhancedScrollerCellView cellViewPrefab;

    [Header("Components")]
    public EnhancedScroller scrollerCraftables;
    public SimpleButton buttonClose;
    public SimpleButton buttonCraft;
    public SuperTextMesh textItem;


    [Header("Logic")]
    public int numberOfCellsPerRow;
    public FurnitureData data;
    UICraftingMaterialDelegate scrollerMaterialDelegate;
    int canCraftMax;

    private UIQuantity qty;
    private UIDialog dialog;

    public override void UIInit()
    {
        scrollerCraftables.ReloadData();
        buttonCraft.ToggleInteractable(false);
    }

    // Use this for initialization
    void Start()
    {
        scrollerCraftables.Delegate = this;

        scrollerMaterialDelegate = GetComponent<UICraftingMaterialDelegate>();

        qty = UIManager.GetMenu<UIQuantity>();
        dialog = UIManager.GetMenu<UIDialog>();
    }

    private void OnEnable()
    {
        buttonClose.onClick += Click_Hide;
        buttonCraft.onClick += Click_Craft;
    }

    private void OnDisable()
    {
        buttonClose.onClick -= Click_Hide;
        buttonCraft.onClick -= Click_Craft;
    }

    public void Click_Hide()
    {
        ChangeState(UIState.HIDE);
    }

    public void Click_Craft()
    {
        Freeze();

        if (GameHelper.GetFurnitureStorage() < Constants.STORAGE_FURNITURE)
        {
            qty.transform.SetAsLastSibling();

            canCraftMax = GameHelper.GetMaxCraftableFurniture(data.id);

            qty.SetMinMax(0, canCraftMax);
            qty.SetOKText(Locale.GetText("sys-ok"));
            qty.SetMessageText(Locale.GetText("craft-qty"));
            qty.onHide -= QuantityCallback;
            qty.onHide += QuantityCallback;
            qty.ChangeState(UIState.SHOW);
        } else
        {
            dialog.transform.SetAsLastSibling();

            dialog.SetMessage(Locale.GetText("craft-storage"));
            dialog.AddChoice(0, Locale.GetText("sys-ok"));
            dialog.SetButtons();
            dialog.onHide -= Unfreeze;
            dialog.onHide += Unfreeze;
            dialog.ChangeState(UIState.SHOW);
        }
    }

    public void Unfreeze()
    {
        dialog.transform.SetAsLastSibling();
        dialog.onHide -= Unfreeze;

        Freeze(false);
        transform.SetAsLastSibling();
    }

    public void QuantityCallback()
    {
        qty.transform.SetAsLastSibling();
        qty.onHide -= QuantityCallback;

        // Take the materials
        foreach (ItemSlot item in data.requiredMaterials)
        {
            GameHelper.ConsumeItem(item.id, item.quantity * UIManager.lastQuantity);
        }

        // Add the furniture
        for (int i = 0; i < UIManager.lastQuantity; i++)
        {
            GameHelper.AddFurniture(data.id);
        }

        ReloadMaterials();
        Unfreeze();
    }

    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        return SaveController.save.unlockedRecipes.Count;
    }

    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        return 260f;
    }

    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        CellViewCraftItem cellView = scroller.GetCellView(cellViewPrefab) as CellViewCraftItem;

        cellView.name = "Cell " + dataIndex.ToString();
        cellView.Refresh(dataIndex * numberOfCellsPerRow);

        return cellView;
    }

    public void ReloadMaterials()
    {
        scrollerMaterialDelegate.scrollerMaterial.ReloadData();
    }
}
