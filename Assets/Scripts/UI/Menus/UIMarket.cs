﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class UIMarket : UIBase
{
    [Header("Components")]
    public SuperTextMesh littleManText;

    public MarketItem buttonSpecial;
    public MarketItem[] buttonFood;
    public MarketItem[] buttonRecipes;
    public MarketItem[] buttonFurniture;

    public UIPopWindow popWindow;

    public SimpleButton buttonClose;


    public void Click_Hide()
    {
        ChangeState(UIState.HIDE);
    }

    public override void UIInit()
    {
        buttonSpecial.marketType = 1;
        buttonSpecial.item = SaveController.save.mktSpecial;
        buttonSpecial.Initialize();
        buttonSpecial.RefreshPrice();
        buttonSpecial.button.Refresh();




        for (int i = 0; i < buttonFood.Length; i++)
        {
            buttonFood[i].marketType = 2;
            buttonFood[i].item = SaveController.save.mktFood[i];
            buttonFood[i].Initialize();
            buttonFood[i].RefreshPrice();
            buttonFood[i].button.Refresh();
        }

        for (int i = 0; i < buttonRecipes.Length; i++)
        {
            buttonRecipes[i].marketType = 3;
            buttonRecipes[i].furniture = SaveController.save.mktRecipes[i];
            buttonRecipes[i].Initialize();
            buttonRecipes[i].RefreshPrice();
            buttonRecipes[i].button.Refresh();
        }

        for (int i = 0; i < buttonFurniture.Length; i++)
        {
            buttonFurniture[i].marketType = 4;
            buttonFurniture[i].furniture = SaveController.save.mktFurniture[i];
            buttonFurniture[i].Initialize();
            buttonFurniture[i].RefreshPrice();
            buttonFurniture[i].button.Refresh();
        }

        UpdatePopWindow();

        littleManText.text = Locale.GetText("mkt-welcome");

    }

    private void OnEnable()
    {
        onShow += StartTextReading;
        buttonClose.onClick += Click_Hide;
    }

    private void OnDisable()
    {
        onShow -= StartTextReading;
        buttonClose.onClick -= Click_Hide;
    }

    public void StartTextReading()
    {
        littleManText.Read();
    }

    public void UpdatePopWindow()
    {
        popWindow.textPops.text = SaveController.save.pops.ToString();
    }
}