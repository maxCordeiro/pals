﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public abstract class UIBase : MonoBehaviour
{
    [Header("Components")]
    public CanvasGroup cGroup;
    public Animator animContr;
    public TextLocalizer[] stmLocalizers;

    public UIBase referrer;

    //public event Action onOpen;
    //public event Action onClose;

    public event Action onPreShow;
    public event Action onPreHide;

    public event Action onShow;
    public event Action onHide;

    [Header("Logic")]
    public bool deselectObject; // Deselects object when opening?

    public bool canInteract;
    public bool isAnimating;
    public bool isVisible;
    public bool clearEvents;

    public UIState state;

    [Header("Settings")]
    public bool openBackground = true;
    public string animNameOpen = "anim_UIBase_Open";
    public string animNameClose = "anim_UIBase_Close";

    private void Awake()
    {
        animContr = GetComponent<Animator>();
        cGroup = GetComponent<CanvasGroup>();
        cGroup.alpha = 0;
        canInteract = false;
    }

    private void OnDisable()
    {
        ClearEvents();
    }

    public void ChangeState(UIState _state)
    {
        state = _state;

        switch (_state)
        {
            case UIState.SHOW:
                onPreShow?.Invoke();
                EnableChildren();
                UIInit();

                foreach (TextLocalizer text in stmLocalizers)
                {
                    text.Initialize();
                }

                Freeze();
                break;
            case UIState.HIDE:
                onPreHide?.Invoke();
                Freeze();
                break;
            default:
                break;
        }

        if (openBackground)
        {
            //GameManager.instance.menuBackground.gameObject.SetActive(true);
            //GameManager.instance.menuBackground.transform.SetSiblingIndex(transform.GetSiblingIndex() - 1);
            Debug.Log("NYI");
        }

        StartCoroutine(PlayAnim());
    }

    public IEnumerator PlayAnim()
    {
        isAnimating = true;

        string animName = "";

        switch (state)
        {
            case UIState.SHOW:
                animName = animNameOpen;
                break;
            case UIState.HIDE:
                animName = animNameClose;
                break;
            default:
                break;
        }

        animContr.Play(animName);
        yield return new WaitForEndOfFrame();

        float clipLength = animContr.GetCurrentAnimatorStateInfo(0).length;

        yield return new WaitForSeconds(clipLength);

        isAnimating = false;

        switch (state)
        {
            case UIState.SHOW:
                isVisible = true;
                Freeze(false);

                onShow?.Invoke();
                break;
            case UIState.HIDE:
                if (referrer != null && !referrer.isVisible)
                {
                    referrer.EnableChildren();
                    referrer.ChangeState(UIState.SHOW);
                    referrer.transform.SetAsLastSibling();
                }

                //if (openBackground) GameManager.instance.menuBackground.gameObject.SetActive(false);
                if (openBackground) Debug.Log("NYI: Menu background");

                isVisible = false;

                EnableChildren(false);
                onHide?.Invoke();

                break;
            default:
                break;
        }

        yield break;
    }

    public void Freeze(bool frozen = true)
    {
        canInteract = !frozen;
        cGroup.interactable = !frozen;
    }

    public void ClearEvents()
    {
        //onOpen = null;
        //onClose = null;

        onPreShow = null;
        onPreHide = null;

        onShow = null;
        onHide = null;
    }

    public void Resume(GameObject selected)
    {
        UIManager.instance.eventSys.SetSelectedGameObject(selected);
        UIManager.instance.eventSys.sendNavigationEvents = true;

        canInteract = true;
    }

    public void EnableChildren(bool active = true)
    {
        foreach (Transform t in transform)
        {
            t.gameObject.SetActive(active);
        }
    }

    public abstract void UIInit();
}