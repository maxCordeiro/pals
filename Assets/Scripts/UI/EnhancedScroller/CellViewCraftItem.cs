﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using EnhancedUI.EnhancedScroller;

public class CellViewCraftItem : EnhancedScrollerCellView
{
    public RowCellViewCraftItem[] rowCellViews;
    HorizontalLayoutGroup horizontalGroup;


    public void Refresh(int startingIndex)
    {
        RectTransform r = GetComponent<RectTransform>();
        horizontalGroup = GetComponent<HorizontalLayoutGroup>();
        if (horizontalGroup != null)  horizontalGroup.spacing = (r.rect.width - (rowCellViews.Length * 260)) / (rowCellViews.Length - 1);

        for (int i = 0; i < rowCellViews.Length; i++)
        {
            rowCellViews[i].Refresh(startingIndex + i < SaveController.save.unlockedRecipes.Count ? SaveController.save.unlockedRecipes[startingIndex + i] : FurnitureID.NONE);
        }
    }
}