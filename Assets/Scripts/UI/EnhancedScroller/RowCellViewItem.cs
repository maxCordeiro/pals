﻿using System.Collections;
using UnityEngine;

public class RowCellViewItem : MonoBehaviour
{
    public SimpleButton simpleButton;
    public SuperTextMesh textQuantity;

    public UIPockets uiPockets;

    public void Refresh(ItemSlot item)
    {
        gameObject.SetActive(item != null);

        if (item == null) return;

        Sprite itemSprite = Resources.Load<Sprite>($"Items/{item.id}");

        if (itemSprite == null) Debug.LogWarning($"{item.id} icon doesn't exist!");

        bool isMaterialItem = item.id.GetData().function == ItemType.MATERIAL;
        
        if (isMaterialItem)
        {
            simpleButton.onClick -= SelectItem;
            simpleButton.onClick += SelectItem;
        }

        simpleButton.spriteIconNormal = itemSprite;
        simpleButton.spriteIconSelect = itemSprite;
        simpleButton.Refresh();

        textQuantity.text = $"x{item.quantity}";
    }

    public void SelectItem()
    {
    }
}