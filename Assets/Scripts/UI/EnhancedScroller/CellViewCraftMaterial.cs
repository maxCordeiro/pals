﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using EnhancedUI.EnhancedScroller;

public class CellViewCraftMaterial : EnhancedScrollerCellView
{
    public Image imageIcon;
    public SuperTextMesh textQuantity;

    public void Refresh(ItemSlot item, int hasQuantity)
    {
        Sprite itemSprite = Resources.Load<Sprite>($"Items/{item.id}");

        if (itemSprite == null) Debug.LogWarning($"{item.id} icon doesn't exist!");

        imageIcon.sprite = itemSprite;

        textQuantity.text = $"{hasQuantity}/{item.quantity}";
    }
}