﻿using UnityEngine;
using UnityEngine.UI;
using EnhancedUI.EnhancedScroller;

public class CellViewFurniture : EnhancedScrollerCellView
{
    public Image imageIcon;

    public void Refresh(FurnitureSlot furniture)
    {
        Sprite furnitureSprite = Resources.Load<Sprite>($"Items/Furniture/{furniture.id}");

        if (furnitureSprite == null) Debug.LogWarning($"{furniture.id} icon doesn't exist!");

        imageIcon.sprite = furnitureSprite;
        imageIcon.SetNativeSize();

        if (imageIcon.rectTransform.sizeDelta.x > 250)
        {
            float xAmt = (250/imageIcon.rectTransform.sizeDelta.x);
            imageIcon.rectTransform.sizeDelta = new Vector2(imageIcon.rectTransform.sizeDelta.x * xAmt, imageIcon.rectTransform.sizeDelta.y * xAmt);
        } else if (imageIcon.rectTransform.sizeDelta.y > 250)
        {
            float yAmt = (250 / imageIcon.rectTransform.sizeDelta.y);
            imageIcon.rectTransform.sizeDelta = new Vector2(imageIcon.rectTransform.sizeDelta.x * yAmt, imageIcon.rectTransform.sizeDelta.y * yAmt);
        }
    }
}