using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EnhancedUI.EnhancedScroller;

public class RowCellViewCraftItem : EnhancedScrollerCellView
{
    public Image imageIcon;
    public Image imageCanCraft;
    public SwitchButton button;

    FurnitureData assignedData;

    public void Refresh(FurnitureID furniture)
    {
        gameObject.SetActive(furniture != FurnitureID.NONE);

        if (furniture == FurnitureID.NONE) return;

        assignedData = furniture.GetData();

        Sprite furnitureSprite = Resources.Load<Sprite>($"Items/Furniture/{furniture}");

        if (furnitureSprite == null) Debug.LogWarning($"{furniture} icon doesn't exist!");

        imageIcon.sprite = furnitureSprite;
        imageIcon.SetNativeSize();

        button.onClick -= AssignRecipeData;
        button.onClick += AssignRecipeData;

        if (imageIcon.rectTransform.sizeDelta.x > 220)
        {
            float xAmt = (220 / imageIcon.rectTransform.sizeDelta.x);
            imageIcon.rectTransform.sizeDelta = new Vector2(imageIcon.rectTransform.sizeDelta.x * xAmt, imageIcon.rectTransform.sizeDelta.y * xAmt);
        }
        else if (imageIcon.rectTransform.sizeDelta.y > 220)
        {
            float yAmt = (220 / imageIcon.rectTransform.sizeDelta.y);
            imageIcon.rectTransform.sizeDelta = new Vector2(imageIcon.rectTransform.sizeDelta.x * yAmt, imageIcon.rectTransform.sizeDelta.y * yAmt);
        }

        imageCanCraft.enabled = GameHelper.CanCraft(furniture);
    }

    public void AssignRecipeData()
    {
        UICrafting ui = UIManager.GetMenu<UICrafting>();

        ui.buttonCraft.ToggleInteractable(button.isSelected && GameHelper.CanCraft(assignedData.id));

        ui.data = !button.isSelected ? new FurnitureData() { id = FurnitureID.NONE } : assignedData;
        ui.textItem.text = !button.isSelected ? "--" : assignedData.id.ToString();
        ui.ReloadMaterials();
    }
}
