﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using EnhancedUI.EnhancedScroller;

public class CellViewItem : EnhancedScrollerCellView
{
    public RowCellViewItem[] rowCellViews;
    HorizontalLayoutGroup horizontalGroup;
    float cachedRectWidth;

    public void Refresh(int pocketIndex, int startingIndex)
    {
        RectTransform r = GetComponent<RectTransform>();

        for (int i = 0; i < rowCellViews.Length; i++)
        {
            rowCellViews[i].Refresh(startingIndex + i < SaveController.save.inventory[pocketIndex].Count ? SaveController.save.inventory[pocketIndex][startingIndex + i] : null);
        }

        if (TryGetComponent(out horizontalGroup))
        {
            int activeChildren = 0;

            for (int i = 0; i < rowCellViews.Length; i++)
            {
                if (rowCellViews[i].gameObject.activeInHierarchy)
                {
                    activeChildren++;
                }
            }

            if (cachedRectWidth == 0)
            {
                cachedRectWidth = r.rect.width;
            }

            if (activeChildren > 1) horizontalGroup.spacing = (cachedRectWidth - (activeChildren * 180f)) / (activeChildren - 1);
        }
    }
}