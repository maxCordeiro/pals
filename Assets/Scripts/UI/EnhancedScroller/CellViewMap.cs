﻿using EnhancedUI.EnhancedScroller;
using UnityEngine;
using UnityEngine.UI;

public class CellViewMap : EnhancedScrollerCellView
{
    [Header("Components")]
    public SuperTextMesh textMapName;
    public SuperTextMesh textMapHour;
    public SwitchButton button;

    public Image imageMap;

    MapData mapData;
    public UIExpeditions uiExpeditions;

    public void Refresh(int _dataIndex)
    {
        button.onClick -= SelectMap;
        button.onClick += SelectMap;

        mapData = ((MapID)_dataIndex).GetData();

        bool selected = UIExpeditions.selectedMap == _dataIndex;

        button.isSelected = selected;
        button.Refresh();

        if (uiExpeditions.expeditionIcons.Length > _dataIndex)
        {
            imageMap.sprite = uiExpeditions.expeditionIcons[_dataIndex];
            button.spriteIconNormal = uiExpeditions.expeditionIcons[_dataIndex];
            button.spriteIconSelect = uiExpeditions.expeditionIcons[_dataIndex];
        }

        textMapName.text = mapData.id.GetText();
        textMapHour.text = $"{mapData.timeToComplete * 60}m";
    }

    public override void RefreshCellView()
    {
        base.RefreshCellView();

        Refresh(dataIndex);
    }

    public void SelectMap()
    {
        UIExpeditions.selectedMap = (int)mapData.id;
        uiExpeditions.scrollerMaps.RefreshActiveCellViews();
    }
}