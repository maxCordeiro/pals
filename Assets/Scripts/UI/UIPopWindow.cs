﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class UIPopWindow : MonoBehaviour
{
    public SuperTextMesh textPops;
    public Image iconPops;

    public void UpdatePops()
    {
        textPops.text = SaveController.save.pops.ToString();
    }
}