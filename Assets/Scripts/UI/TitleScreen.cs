﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.UI;
using UnityEngine;
using ScreenTransitionsPro;

public class TitleScreen : MonoBehaviour
{
    public SimpleButton buttonNew;
    public SimpleButton buttonLoad;

    private SuperTextMesh textNew;
    private SuperTextMesh textLoad;

    // These only exist because I thought I would need
    // more control over which scripts are subscribing to init events
    public List<GameObject> objectInstantiateOrderPreSave;
    public List<GameObject> objectInstantiateOrderPostSave;
    private List<GameObject> preSaveObjects;
    private List<GameObject> postSaveObjects;


    private void Awake()
    {
        buttonNew.gameObject.SetActive(false);
        buttonLoad.gameObject.SetActive(false);

        textNew = buttonNew.transform.GetChild(0).GetComponent<SuperTextMesh>();
        textLoad = buttonLoad.transform.GetChild(0).GetComponent<SuperTextMesh>();

        // Also should be in its own object probably
        preSaveObjects = new List<GameObject>();
        postSaveObjects = new List<GameObject>();

        for (int i = 0; i < objectInstantiateOrderPreSave.Count; i++)
        {
            preSaveObjects.Add(Instantiate(objectInstantiateOrderPreSave[i]));
        }
    }

    private void Start()
    {
        // This shouldn't be here
        ScreenTransitionFade fade = Camera.main.GetComponent<ScreenTransitionFade>();
        fade.SetMaterial(Instantiate(fade.transitionMaterial));

        EventManager.InitializeGarden();
    }


    private void OnEnable()
    {
        buttonNew.onClick += InitNewGame;
        buttonLoad.onClick += InitLoad;

        EventManager.onInitializeGarden += EventManager_onInitializeGarden;
    }

    private void EventManager_onInitializeGarden()
    {
        textNew.text = Locale.GetText("title-newgame");
        textLoad.text = Locale.GetText("title-loadgame");

        buttonNew.gameObject.SetActive(true);
        buttonLoad.gameObject.SetActive(true);
    }

    private void OnDisable()
    {
        buttonNew.onClick -= InitNewGame;
        buttonLoad.onClick -= InitLoad;

        EventManager.onInitializeGarden -= EventManager_onInitializeGarden;
    }

    private void InitNewGame()
    {
        InitGardenScene(false);
    }

    private void InitLoad()
    {
        InitGardenScene(true);
    }

    private void InitGardenScene(bool loadGame)
    {
        buttonNew.ToggleInteractable(false);
        buttonLoad.ToggleInteractable(false);

        if (loadGame)
        {
            EventManager.LoadGame();
        } else
        {
            EventManager.NewGame();
        }

        for (int i = 0; i < objectInstantiateOrderPostSave.Count; i++)
        {
            postSaveObjects.Add(Instantiate(objectInstantiateOrderPostSave[i]));
        }

        EventManager.inGarden = true;

        EventManager.MinutePass((int)Clock.minutesPassed);
        EventManager.RefreshHUD();
        EventManager.ToggleHUDButtons(true, true);
        EventManager.ToggleHUDStatus(SaveController.save.pal.ageID != PalAges.EGG && !SaveController.save.pal.isOnExpedition); // Called since HUD.cs's LoadGame event doesn't get fired
        EventManager.ToggleCameraDrag(true);
        EventManager.LoadFurniture(false);

        Destroy(gameObject);
    }
}
