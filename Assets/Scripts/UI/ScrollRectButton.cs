using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EnhancedUI.EnhancedScroller;
using UnityEngine.EventSystems;

public class ScrollRectButton : Button, IDragHandler, IBeginDragHandler, IEndDragHandler
{
    public ScrollRect scrollRect;

    bool isDragging;

    public new void Start()
    {
        base.Start();

        if (scrollRect == null)
        {
            scrollRect = transform.parent.GetComponentInParent<ScrollRect>();
        }
    }

    public override void OnPointerEnter(PointerEventData eventData)
    {

    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        DoStateTransition(SelectionState.Normal, true);

        if (!isDragging)
        {
            DoClick();
        }
    }

    public override void OnPointerDown(PointerEventData eventData)
    {
        DoStateTransition(SelectionState.Pressed, true);
    }

    public override void OnPointerClick(PointerEventData eventData)
    {

    }

    void DoClick()
    {
        onClick?.Invoke();
    }
    
    public void OnBeginDrag(PointerEventData eventData)
    {
        isDragging = true;
        DoStateTransition(SelectionState.Normal, true);
        scrollRect.OnBeginDrag(eventData);
    }
    
    public void OnDrag(PointerEventData eventData)
    {
        scrollRect.OnDrag(eventData);
    }
    
    public void OnEndDrag(PointerEventData eventData)
    {
        isDragging = false;
        scrollRect.OnEndDrag(eventData);
    }
}
