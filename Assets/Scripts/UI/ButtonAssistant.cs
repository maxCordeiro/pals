﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonAssistant : MonoBehaviour, IPointerUpHandler, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IPointerExitHandler
{
    [Header("Toggle")]
    public bool isToggled;

    [Header("Switch")]
    public bool isSwitched;


    [Header("Icon")]
    public Sprite iconDefault;
    public Sprite iconSelect;

    public Color colorDefaut;
    public Color colorImagePointerDown;
    public Color colorSelect;

    [Header("Button")]
    public Sprite buttonDefault;
    public Sprite buttonSelected;

    public Color colorButtonNormal;
    public Color colorButtonPointerDown;
    public Color colorButtonSelect;

    Image imageButton;
    Image imageIcon;

    Button btn;

    [Header("Logic")]
    public bool inScroll;
    //public ScrollRect scroller;

    public delegate void OnToggle();
    //public event OnToggle onToggleEvent;

    bool isDragging;

    public enum ButtonBehavior
    {
        TOGGLEICON,
        NORMALICON,
        NORMAL,
        SWITCH
    }

    public ButtonBehavior behavior;

    [Header("Components")]
    public GameObject toggleParent;
    ButtonAssistant[] toggleButtons;

    private void Awake()
    {
        btn = GetComponent<Button>();
        imageButton = GetComponent<Image>();

        if (behavior != ButtonBehavior.NORMALICON && behavior != ButtonBehavior.TOGGLEICON) return;

        imageIcon = transform.GetChild(0).GetComponent<Image>();
        imageIcon.sprite = iconDefault;
        imageIcon.color = colorDefaut;
    }

    private void Start()
    {
        if (behavior == ButtonBehavior.TOGGLEICON)
        {
            if (toggleParent == null) return;

            toggleButtons = new ButtonAssistant[toggleParent.transform.childCount];

            for (int i = 0; i < toggleButtons.Length; i++)
            {
                toggleButtons[i] = toggleParent.transform.GetChild(i).GetComponent<ButtonAssistant>();
            }
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        switch (behavior)
        {
            case ButtonBehavior.TOGGLEICON:
                if (inScroll && isDragging) break;

                if (!isToggled && toggleParent != null)
                {
                    foreach (ButtonAssistant toggle in toggleButtons)
                    {
                        toggle.isToggled = false;
                        toggle.RefreshSelected();
                    }
                }

                isToggled = true;
                //onToggleEvent?.Invoke();

                RefreshSelected();
                break;
            case ButtonBehavior.NORMALICON:
            case ButtonBehavior.NORMAL:

                imageButton.color = colorButtonSelect;

                if (behavior == ButtonBehavior.NORMAL) break;

                imageIcon.sprite = iconDefault;
                imageIcon.color = colorDefaut;
                imageIcon.SetNativeSize();
                break;
            case ButtonBehavior.SWITCH:
                imageButton.color = colorButtonSelect;

                imageIcon.sprite = iconDefault;
                imageIcon.color = colorDefaut;
                imageIcon.SetNativeSize();
                break;
            default:
                break;
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        switch (behavior)
        {
            case ButtonBehavior.TOGGLEICON:
                imageButton.sprite = buttonSelected;
                imageButton.color = colorButtonPointerDown;

                imageIcon.color = colorImagePointerDown;
                imageIcon.SetNativeSize();
                break;
            case ButtonBehavior.NORMALICON:
            case ButtonBehavior.NORMAL:
                imageButton.sprite = buttonSelected;
                imageButton.color = colorButtonPointerDown;

                if (behavior == ButtonBehavior.NORMAL) break;

                imageIcon.color = colorImagePointerDown;
                break;
            default:
                break;
        }
    }

    public void RefreshSelected()
    {
        switch (behavior)
        {
            case ButtonBehavior.TOGGLEICON:
                break;

            case ButtonBehavior.SWITCH:
                break;
            default:
                break;
        }

        imageButton.sprite = isToggled ? buttonSelected : buttonDefault;
        imageButton.color = isToggled ? colorButtonSelect : colorButtonNormal;

        if (behavior == ButtonBehavior.NORMAL) return;
        imageIcon.sprite = isToggled ? iconSelect : iconDefault;
        imageIcon.color = isToggled ? colorSelect : colorDefaut;
        imageIcon.SetNativeSize();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (inScroll)
        {
            isDragging = true;
            RefreshSelected();
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (inScroll)
        {
            isDragging = false;
            RefreshSelected();
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        
    }
}