using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextLocalizer : MonoBehaviour
{
    public string key;
    SuperTextMesh stm;

    private void Awake()
    {
        stm = GetComponent<SuperTextMesh>();
    }

    private void Start()
    {
        Initialize();
    }

    public void Initialize()
    {
        stm.text = Locale.GetText(key);
    }
}
