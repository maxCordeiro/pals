using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class FurnitureButton : ScrollRectButton, IPointerDownHandler, IPointerUpHandler, IBeginDragHandler
{
    public event Action onTimerTrigger;
    public void OnTimerTrigger() => onTimerTrigger?.Invoke();

    CellViewFurniture cellView;
    UIDecoration uiDeco;

    public new void Start()
    {
        base.Start();
        cellView = GetComponent<CellViewFurniture>();

        if (Application.isPlaying)
        {
            uiDeco = transform.parent.parent.parent.parent.parent.parent.GetComponent<UIDecoration>();
        }
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        onTimerTrigger += TimerTrigger;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        onTimerTrigger -= TimerTrigger;
    }

    public void TimerTrigger()
    {
        DoStateTransition(SelectionState.Normal, true);
        //timerTriggered = true;

        uiDeco.ChangeState(UIState.HIDE);

        EventManager.DecorateFurniture(cellView.dataIndex, SaveController.save.inventoryFurniture[(int)uiDeco.selectedFurnitureType][cellView.dataIndex].id);

        //GameManager.instance.colliderFurniturePreview.offset = data.boxOffset;
        //GameManager.instance.colliderFurniturePreview.size = data.boxSize;
        //
        //GameManager.instance.spriteFurniturePreview.sprite = SaveController.save.inventoryFurniture[(int)uiDeco.selectedFurnitureType][cellView.dataIndex].id.GetSprite();
        //GameManager.instance.lastSelectedFurnitureIndex = cellView.dataIndex;
        //GameManager.instance.lastSelectedFurnitureType = uiDeco.selectedFurnitureType;
        //GameManager.instance.isHoldingCounterFurniture = data.canBePlacedOnCounter;
        //GameManager.instance.isDraggingFurniture = true;
    }

    public new void OnBeginDrag(PointerEventData eventData)
    {
        Vector3 dragDir = (eventData.position - eventData.pressPosition).normalized;

        if (GetDragDirection(dragDir) == DraggedDirection.Up)
        {
            OnTimerTrigger();
        } else
        {
            base.OnBeginDrag(eventData);
        }
    }

    public override void OnPointerDown(PointerEventData eventData)
    {
        base.OnPointerDown(eventData);
    }

    private enum DraggedDirection
    {
        Up,
        Down,
        Right,
        Left
    }

    private DraggedDirection GetDragDirection(Vector3 dragVector)
    {
        float positiveX = Mathf.Abs(dragVector.x);
        float positiveY = Mathf.Abs(dragVector.y);

        DraggedDirection draggedDir;

        if (positiveX > positiveY)
        {
            draggedDir = (dragVector.x > 0) ? DraggedDirection.Right : DraggedDirection.Left;
        }
        else
        {
            draggedDir = (dragVector.y > 0) ? DraggedDirection.Up : DraggedDirection.Down;
        }

        return draggedDir;
    }
}
