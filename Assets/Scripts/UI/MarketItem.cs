﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.UI;
using UnityEngine;

public class MarketItem : MonoBehaviour
{
    // 0 - Nothing
    // 1 - Daily Special
    // 2 - Food
    // 3 - Recipes
    // 4 - Furniture
    public int marketType;
    
    public ItemID item;
    public FurnitureID furniture;

    public int price;

    public SuperTextMesh textPrice;

    public SimpleButton button;

    public int itemIndex;

    private UIDialog dialog;

    private void Start()
    {
        dialog = UIManager.GetMenu<UIDialog>();
    }

    private void OnEnable()
    {
        button.onClick += OpenBuyDialog;
    }

    private void OnDisable()
    {
        button.onClick -= OpenBuyDialog;
    }

    public void OpenBuyDialog()
    {
        dialog.transform.SetAsLastSibling();

        dialog.AddChoice(0, Locale.GetText("mkt-buy"));
        dialog.AddChoice(1, Locale.GetText("mkt-no"));
        dialog.SetButtons();
        dialog.SetMessage(Locale.GetTextFormat("mkt-buymsg", item.GetText(), price.ToString()));
        dialog.onHide -= DialogCallback;
        dialog.onHide += DialogCallback;
        dialog.ChangeState(UIState.SHOW);
    }

    public void DialogCallback()
    {
        switch (UIManager.lastSelectedDialog)
        {
            case 0:

                if (SaveController.save.pops >= price)
                {
                    dialog.transform.SetAsLastSibling();
                    dialog.onHide -= DialogCallback;

                    button.ToggleInteractable(false);

                    GameHelper.AddItem(item);
                    GameHelper.AddPops(-price);

                    UIManager.GetMenu<UIMarket>().UpdatePopWindow();
                } else
                {
                    dialog.transform.SetAsLastSibling();
                    dialog.AddChoice(0, Locale.GetText("sys-okne"));
                    dialog.SetButtons();
                    dialog.SetMessage(Locale.GetText("mkt-nopops"));
                    dialog.onHide -= DialogCallback;
                    dialog.ChangeState(UIState.SHOW);
                }
                break;
            case 1:
                break;
            default:
                break;
        }
    }

    public void Initialize()
    {
        switch (marketType)
        {
            case 1:
            case 2:
                button.spriteIconNormal = item.GetSprite();
                button.spriteIconSelect = item.GetSprite();
                break;
            case 3:
            case 4:
                button.spriteIconNormal = furniture.GetSprite();
                button.spriteIconSelect = furniture.GetSprite();
                
                // Early refresh to initialize icon size
                button.Refresh();
                button.imageIcon.SetNativeSize();

                if (button.imageIcon.rectTransform.sizeDelta.x > 216)
                {
                    float xAmt = (216 / button.imageIcon.rectTransform.sizeDelta.x);
                    button.imageIcon.rectTransform.sizeDelta = new Vector2(button.imageIcon.rectTransform.sizeDelta.x * xAmt, button.imageIcon.rectTransform.sizeDelta.y * xAmt);
                }
                else if (button.imageIcon.rectTransform.sizeDelta.y > 216)
                {
                    float yAmt = (216 / button.imageIcon.rectTransform.sizeDelta.y);
                    button.imageIcon.rectTransform.sizeDelta = new Vector2(button.imageIcon.rectTransform.sizeDelta.x * yAmt, button.imageIcon.rectTransform.sizeDelta.y * yAmt);
                }

                break;
            default:
                break;
        }
    }

    public void RefreshPrice()
    {
        switch (marketType)
        {
            case 1:
            case 2:
                ItemData iData = item.GetData();
                price = Mathf.RoundToInt(iData.sellPrice * 2.5f);
                break;
            case 3:
            case 4:
                price = 0;

                FurnitureData fData = furniture.GetData();
                
                for (int i = 0; i < fData.requiredMaterials.Count; i++)
                {
                    ItemData mData = fData.requiredMaterials[i].id.GetData();
                    price += Mathf.RoundToInt(mData.sellPrice * 2.5f) * fData.requiredMaterials[i].quantity;
                }

                break;

            default:
                break;
        }
        textPrice.text = Locale.GetTextFormat("mkt-price", price.ToString());
    }
}