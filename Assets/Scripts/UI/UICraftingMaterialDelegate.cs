﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using EnhancedUI.EnhancedScroller;

public class UICraftingMaterialDelegate : MonoBehaviour, IEnhancedScrollerDelegate
{
    public EnhancedScrollerCellView cellViewPrefab;

    [Header("Components")]
    public EnhancedScroller scrollerMaterial;
    UICrafting uiCrafting;

    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        CellViewCraftMaterial cellView = scrollerMaterial.GetCellView(cellViewPrefab) as CellViewCraftMaterial;

        cellView.name = "Cell " + dataIndex.ToString();
        ItemSlot item = uiCrafting.data.requiredMaterials[dataIndex];

        int hasQuantity = 0;

        if (GameHelper.HasItem(item.id))
        {
            hasQuantity = SaveController.save.inventory[GameHelper.GetItemPocketIndex(item.id)][GameHelper.GetItemInventoryIndex(item.id)].quantity;
        }

        cellView.Refresh(item, hasQuantity);

        return cellView;
    }

    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        return 140f;
    }

    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        return uiCrafting.data.id == FurnitureID.NONE ? 0 : uiCrafting.data.requiredMaterials.Count;
    }

    public int GetTotalCanCraft()
    {
        int canCraft = 0;



        return canCraft;
    }

    private void Start()
    {
        uiCrafting = GetComponent<UICrafting>();

        scrollerMaterial.Delegate = this;
    }
}    