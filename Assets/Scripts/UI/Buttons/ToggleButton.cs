﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ToggleButton : ButtonBase, IPointerUpHandler, IBeginDragHandler, IEndDragHandler, IPointerExitHandler
{
    [Header("Logic")]
    public bool isToggled;
    public GameObject toggleParent;
    ToggleButton[] toggleButtons;
    
    [Header("Config (Base)")]
    public bool defineTogglesOnAwake; //When instantiating in a for loop, this needs to be turned off

    public event Action onToggle;
    public void OnToggle() => onToggle?.Invoke();


    public new void Awake()
    {
        base.Awake();

        if (defineTogglesOnAwake) DefineToggleButtons();
    }

    public void DefineToggleButtons()
    {
        toggleButtons = new ToggleButton[toggleParent.transform.childCount];

        for (int i = 0; i < toggleButtons.Length; i++)
        {
            toggleButtons[i] = toggleParent.transform.GetChild(i).GetComponent<ToggleButton>();
        }
    }

    private void OnDestroy()
    {
        onToggle = null;
    }

    public new void OnPointerUp(PointerEventData eventData)
    {
        if (!IsInteractable()) return;
        pointerDown = false;

        if (inScroller && isDragging)
        {
            Refresh();
            return;
        }


        if (hasExit)
        {
            hasExit = false;
            Refresh();
            return;
        }

        if (!isToggled)
        {
            foreach (ToggleButton toggle in toggleButtons)
            {
                toggle.isToggled = false;
                toggle.Refresh();
            }

            isToggled = true;
            OnToggle();
            Refresh();
        }

        Refresh();
    }

    public override void Refresh()
    {
        imageButton.sprite = ((isToggled || pointerDown) && !isDragging) ? spriteButtonSelect : spriteButtonNormal;
        if (setNativeSizes) imageButton.SetNativeSize();
        imageButton.color = ((isToggled || pointerDown) && !isDragging) ? colorButtonSelect : colorButtonNormal;

        if (!hasIcon) return;

        imageIcon.sprite = isToggled ? spriteIconSelect : spriteIconNormal;
        if (setNativeSizes) imageIcon.SetNativeSize();
        imageIcon.color = ((isToggled || pointerDown) && !isDragging) ? colorIconSelect : colorIconNormal;
    }
}