﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using System;

public class DialogButton : SimpleButton
{
    [Header("Components")]
    public SuperTextMesh buttonText;
    public UIDialog uiDialog;

    [Header("Logic")]
    public int id;

    private void OnEnable()
    {
        RemoveAllListeners();
        onClick += ButtonClick;
    }

    private void OnDisable()
    {
        onClick -= ButtonClick;
    }

    public void ButtonClick()
    {
        UIDialog dialog = UIManager.GetMenu<UIDialog>();

        if (dialog.isAnimating) return;

        UIManager.lastSelectedDialog = id;
        dialog.ChangeState(UIState.HIDE);
    }
}
