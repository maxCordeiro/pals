﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public abstract class ButtonBase : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler, IBeginDragHandler, IEndDragHandler, IPointerExitHandler
{
    [Header("Button")]
    public Sprite spriteButtonNormal;
    public Sprite spriteButtonSelect;

    public Color colorButtonNormal;
    public Color colorButtonSelect;

    [Header("Icon")]
    public Sprite spriteIconNormal;
    public Sprite spriteIconSelect;

    public Color colorIconNormal;
    public Color colorIconSelect;

    [Header("Components")]
    public Image imageButton;
    public Image imageIcon;

    [Header("Logic (Base)")]
    public bool pointerDown; // If the pointer has pressed down on the Button
    public bool hasExit; // If the pointer has exit the Button while pressing down
    public bool isDragging; // If the pointer is dragging on the Button
    public bool interactable;

    [Header("Config (Base)")]
    public bool inScroller; // If the button is in a ScrollRect
    public bool hasIcon; // Disable if the button doesn't have an icon
    public bool setNativeSizes; // Whether to use SetNativeSize() on Button and icon images

    private UIBase uiBaseParent;
    private CanvasGroup cGroup;
    public ScrollRect scrollRect;

    public event System.Action onClick;

    public void Awake()
    {
        DefineCanvasGroup();

        imageButton = GetComponent<Image>();
        if (hasIcon) imageIcon = transform.GetChild(0).GetComponent<Image>();

        // Might need to remove this
        interactable = true;

        if (scrollRect == null)
        {
            scrollRect = transform.parent.GetComponentInParent<ScrollRect>();
        }
    }

    public void DefineCanvasGroup()
    {
        if (cGroup == null)
        {
            uiBaseParent = GetComponentInParent<UIBase>(true);

            if (uiBaseParent != null)
            {
                cGroup = uiBaseParent.GetComponent<CanvasGroup>();
            }
            else
            {
                cGroup = GetComponentInParent<CanvasGroup>();
            }
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (!IsInteractable()) return;
        
        pointerDown = true;
        Refresh();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (!IsInteractable()) return;

        pointerDown = false;

        if (hasExit)
        {
            hasExit = false;
            Refresh();
            return;
        }

        Refresh();

        if (!isDragging)
        {
            onClick?.Invoke();
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        isDragging = true;
        if (scrollRect != null) scrollRect.OnBeginDrag(eventData);
        Refresh();
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (scrollRect != null) scrollRect.OnDrag(eventData);
        isDragging = true;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        isDragging = false;
        if (scrollRect != null) scrollRect.OnEndDrag(eventData);
        Refresh();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (!pointerDown || inScroller) return;

        hasExit = true;
    }

    public void ToggleInteractable(bool _interactable, bool refresh = true)
    {
        interactable = _interactable;
        if (refresh) Refresh();
    }

    public bool IsInteractable()
    {
        if (cGroup == null) return interactable;

        return cGroup.interactable && interactable;
    }

    public void OnClick()
    {
        onClick?.Invoke();
    }

    public void RemoveAllListeners()
    {
        onClick = null;
    }

    public abstract void Refresh();
}