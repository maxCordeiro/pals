﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

// Button attachment to be used for garden hud and main menu buttons
public class SwitchButton : ButtonBase, IPointerUpHandler
{
    [Header("Logic")]
    public bool isSelected;

    public event System.Action onSelected;

    public new void OnPointerUp(PointerEventData eventData)
    {
        if (!IsInteractable()) return;

        pointerDown = false;

        if (inScroller && isDragging)
        {
            Refresh();
            return;
        }

        if (hasExit)
        {
            hasExit = false;
            Refresh();
            return;
        }

        isSelected = !isSelected;

        if (isSelected) onSelected?.Invoke();
        OnClick();
        Refresh();
    }

    private void OnDisable()
    {
        onSelected = null;
    }

    public override void Refresh()
    {
        imageButton.sprite = ((isSelected || pointerDown) && !isDragging) ? spriteButtonSelect : spriteButtonNormal;
        if (setNativeSizes) imageButton.SetNativeSize();
        imageButton.color = interactable ? pointerDown ? colorButtonSelect : colorButtonNormal : new Color(colorButtonNormal.r, colorButtonNormal.g, colorButtonNormal.b, 0.5f);

        if (!hasIcon) return;

        imageIcon.sprite = isSelected && !isDragging ? spriteIconSelect : spriteIconNormal;
        if (setNativeSizes) imageIcon.SetNativeSize();
        imageIcon.color = ((isSelected || pointerDown) && !isDragging) ? colorIconSelect: colorIconNormal;
    }
}