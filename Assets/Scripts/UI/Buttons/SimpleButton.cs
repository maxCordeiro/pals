using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SimpleButton : ButtonBase
{
    public override void Refresh()
    {
        imageButton.sprite = pointerDown ? spriteButtonSelect : spriteButtonNormal;
        if (setNativeSizes) imageButton.SetNativeSize();
        imageButton.color = interactable ? pointerDown ? colorButtonSelect : colorButtonNormal : new Color(colorButtonNormal.r, colorButtonNormal.g, colorButtonNormal.b, 0.5f);

        if (!hasIcon) return;
        imageIcon.sprite = pointerDown ? spriteIconSelect : spriteIconNormal ;
        if (setNativeSizes) imageIcon.SetNativeSize();
        imageIcon.color = interactable ? pointerDown ? colorIconSelect : colorIconNormal : new Color(colorIconNormal.r, colorIconNormal.g, colorIconNormal.b, 0.25f); ;
    }
}
