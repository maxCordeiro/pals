﻿using UnityEngine;
using System.Collections;

public class FurnitureEntity : MonoBehaviour
{
    [Header("Components")]
    public SpriteRenderer sprite;
    public BoxCollider2D boxCollider;
    public BoxCollider2D boxCounterCollider;

    // Data
    public FurnitureSlot furniture;

    public bool isDragging;
    public bool isCounterItem;

    public void Initialize()
    {
        name = $"Furniture ({furniture.id} [{furniture.color}])";

        LoadSprite();
        LoadCollision();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void LoadSprite()
    {
        Sprite furnitureSprite = Resources.Load<Sprite>($"Items/Furniture/{furniture.id}");

        if (furnitureSprite == null) Debug.LogWarning($"{furniture.id} sprite doesn't exist!");

        sprite.sprite = furnitureSprite;
    }

    public void LoadCollision()
    {
        FurnitureData data = furniture.id.GetData();

        boxCollider.offset = data.boxOffset;
        boxCollider.size = data.boxSize;

        if (data.isCounter)
        {
            boxCounterCollider.enabled = true;
            isCounterItem = true;
            boxCounterCollider.offset = data.boxCounterOffset;
            boxCounterCollider.size = data.boxCounterSize;
        } else
        {
            boxCounterCollider.enabled = false;
            isCounterItem = false;
        }
    }

    public void SyncPositions()
    {
        furniture.position = transform.position;
    }
}
