using Newtonsoft.Json;
using System;
using UnityEngine;
using System.Collections.Generic;

[Serializable]
public class FurnitureSlot {
    public FurnitureID id;

    public int color; // not yet implemented

    public bool isOnCounter;

    // Save info
    public int saveIndex;
    public Vector2 position;
}