using System;
using System.Collections.Generic;
using UnityEngine;

public struct FurnitureData {
    public FurnitureID id;
    public FurnitureType type;

    // For Box Collider 2D overrides
    public Vector2 boxSize;
    public Vector2 boxOffset;

    // Counter fields
    public bool isCounter;
    public bool canBePlacedOnCounter;

    public Vector2 boxCounterSize;
    public Vector2 boxCounterOffset;

    // Recipe
    public List<ItemSlot> requiredMaterials;
}