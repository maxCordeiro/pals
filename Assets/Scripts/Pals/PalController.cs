﻿using System.Collections.Generic;
using UnityEngine;

public class PalController : MonoBehaviour
{
    [Header("Components")]
    public SpriteRenderer spriteRenderer;
    public SpriteRenderer spriteShadow;

    public SpriteAnimator spriteAnim;

    // Use this for initialization
    private void Start()
    {
        //spriteAnim.Play(0);
    }

    private void OnEnable()
    {
        EventManager.onMinutePass += EventManager_onMinutePass;
        EventManager.onCompleteExpedition += EventManager_onCompleteExpedition;
        EventManager.onNewGame += EventManager_onNewGame;
        EventManager.onRefreshHUD += EventManager_onRefreshHUD;
    }

    private void EventManager_onRefreshHUD()
    {
        RefreshSprite();
    }

    private void OnDisable()
    {
        EventManager.onMinutePass -= EventManager_onMinutePass;
        EventManager.onCompleteExpedition -= EventManager_onCompleteExpedition;
        EventManager.onNewGame -= EventManager_onNewGame;
        EventManager.onRefreshHUD -= EventManager_onRefreshHUD;
    }

    private void EventManager_onMinutePass(int obj)
    {
        SaveController.save.pal.TickMinute(obj);
    }

    private void EventManager_onCompleteExpedition()
    {
        List<ItemSlot> loot = GameHelper.GetExpeditionLoot(SaveController.save.pal.expeditionMap);
        int pops = GameHelper.GetExpeditionPops((MapID)UIExpeditions.selectedMap, loot);
        SaveController.save.pal.UseStamina(Mathf.FloorToInt(((MapID)UIExpeditions.selectedMap).GetData().timeToComplete * 10));

        EventManager.RefreshHUD();

        for (int i = 0; i < loot.Count; i++)
        {
            GameHelper.AddItem(loot[i]);
        }

        GameHelper.AddPops(pops);
    }

    private void RefreshSprite()
    {
        bool onExpedition = SaveController.save.pal.isOnExpedition;

        if (onExpedition)
        {
            if (spriteRenderer.enabled)
            {
                LeanTween.alpha(spriteRenderer.gameObject, 0, 0.1f);
                LeanTween.alpha(spriteShadow.gameObject, 0, 0.1f);
                LeanTween.moveY(gameObject, -1, 0.2f).setOnComplete(() =>
                {
                    spriteRenderer.enabled = false;
                    spriteShadow.enabled = false;
                });
            }
        } else
        {
            if (!spriteRenderer.enabled)
            {
                spriteRenderer.enabled = true;
                spriteShadow.enabled = true;
                LeanTween.alpha(spriteRenderer.gameObject, 1, 0.1f);
                LeanTween.alpha(spriteShadow.gameObject, 0.2f, 0.1f);
                LeanTween.moveY(gameObject, 0, 0.2f);
            }

            spriteAnim.Play(0);
        }

        bool isEgg = SaveController.save.pal.ageID == PalAges.EGG;

        if (isEgg)
        {
            Sprite eggSprite = Resources.Load<Sprite>("Pals/EGG_0");
            spriteRenderer.sprite = eggSprite;
            spriteAnim.animations[0].frames[0].frameSprite = eggSprite;
            spriteAnim.animations[0].frames[1].frameSprite = eggSprite;
        } else
        {
            Sprite[] palSprites = GetPalSprites(SaveController.save.pal.id);

            spriteRenderer.sprite = palSprites[0];
            spriteAnim.animations[0].frames[0].frameSprite = palSprites[0];
            spriteAnim.animations[0].frames[1].frameSprite = palSprites[1];
        }

    }


    private void EventManager_onNewGame()
    {
        SaveController.save.pal = Pal.GeneratePal();
    }

    public static Sprite[] GetPalSprites(PalID id)
    {
        Sprite[] palSprites = Resources.LoadAll<Sprite>($"Pals/{id}/{id}_idle");

        if (palSprites.Length > 0)
        {
            return palSprites;
        }

        return Resources.LoadAll<Sprite>("Pals/missingno_idle");
    }
}