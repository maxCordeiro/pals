﻿using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

[System.Serializable]
public class Pal
{
    public PalID id;
    public string palName;

    public int stamina; // Goes down every expedition
    public int hunger; // Goes down every 30 minutes
    public int happiness; // Goes down every 45 minutes

    public int hungerInterval;
    public int happinessInterval;

    public int ageCounter; // Range from 0 - AGECAP, Goes up every day
    public PalAges ageID;

    public bool isOnExpedition;
    public MapID expeditionMap;
    public float expeditionTimer;

    public bool isSleeping;

    public bool isSick; // 5% chance to get sick after every expedition

    public Pal(PalID _id)
    {
        id = _id;

        stamina = Constants.CAP_STAMINA;
        hunger = Constants.CAP_HUNGER;
        happiness = Constants.CAP_HAPPINESS;

        palName = "Pal";

        ageCounter = 0;
        ageID = PalAges.EGG;
    }

    private Pal()
    {

    }

    public int GetPower()
    {
        return (hunger + happiness + (ageCounter - (Constants.CAP_AGE * Constants.MINUTES_IN_A_DAY))) / 3;
    }

    public void TickAge(int minMultiplier = 1)
    {
        ageCounter = Mathf.Clamp(ageCounter + (1 * minMultiplier), 0, Constants.CAP_AGE * Constants.MINUTES_IN_A_DAY);

        if (ageID == PalAges.EGG)
        {
            if (ageCounter >= 5)
            {
                ageID = PalAges.SPROUT;
                ageCounter -= 5;
            }
        }



        if (ageCounter >= Constants.AGE_OLDIE * Constants.MINUTES_IN_A_DAY)
        {
            if (ageCounter >= Constants.CAP_AGE * Constants.MINUTES_IN_A_DAY)
            {
                ageID = PalAges.DEAD;
            } else
            {
                ageID = PalAges.OLDIE;
            }
        } else if (ageCounter >= Constants.AGE_ADULT * Constants.MINUTES_IN_A_DAY)
        {
            ageID = PalAges.ADULT;
        } else if (ageCounter >= Constants.AGE_YOUNGIN * Constants.MINUTES_IN_A_DAY)
        {
            ageID = PalAges.YOUNGIN;
        }
    }

    public void Feed(int amt)
    {
        hunger = Mathf.Clamp(hunger + amt, 0, Constants.CAP_HUNGER);
    }

    public void TickMinute(int minutesPassed = 1)
    {
        /*bool sleepCheck = SleepCheck();

        if (sleepCheck)
        {
            if (!GameManager.instance.timeFellAsleepMarked)
            {
                GameManager.instance.timeFellAsleep = DateTime.Now;
                GameManager.instance.timeFellAsleepMarked = true;
            }
            isSleeping = sleepCheck;
        } else
        {*/

        if (ageID != PalAges.DEAD)
        {            
            TickAge(minutesPassed);

            if (ageID != PalAges.EGG && !isOnExpedition)
            {
                TickHunger(minutesPassed);
                TickHappiness(minutesPassed);
            }
            if (isOnExpedition) TickExpedition(minutesPassed);
        }

        //}
    }

    public void TickDay(int daysPassed = 1)
    {
        TickAge(daysPassed);
    }

    public void TickHappiness(int minMultiplier = 1)
    {
        happinessInterval += minMultiplier;
        
        if (happinessInterval >= Constants.INTERVAL_HAPPINESS)
        {
            int times = Mathf.FloorToInt(happinessInterval / Constants.INTERVAL_HAPPINESS);

            happiness = Mathf.Clamp(happiness - (Constants.TICK_HAPPINESS * times), 0, Constants.CAP_HAPPINESS);
            happinessInterval -= times * Constants.INTERVAL_HAPPINESS;
        }
    }

    public void TickHunger(int minMultiplier = 1)
    {
        hungerInterval += minMultiplier;

        if (hungerInterval >= Constants.INTERVAL_HUNGER)
        {
            int times = Mathf.FloorToInt(hungerInterval / Constants.INTERVAL_HUNGER);

            hunger = Mathf.Clamp(hunger - (Constants.TICK_HUNGER * times), 0, Constants.CAP_HUNGER);

            hungerInterval -= times * Constants.INTERVAL_HUNGER;

            if (hunger <= Constants.THRESHOLD_HUNGER)
            {
                // Pal fucking dies
            }
        }
    }

    public void TickExpedition(int minMultiplier = 1)
    {
        expeditionTimer -= minMultiplier;

        if (expeditionTimer <= 0)
        {
            isOnExpedition = false;
            expeditionTimer = 0;
            EventManager.CompleteExpedition();
        }
    }
    
    public void RollSickness()
    {
        isSick = Random.Range(0f, 1f) <= 0.05;

        if (isSick)
        {
            //GameManager.instance.palEntity.
        }
    }

    public bool SleepCheck()
    {
        //DateTime currentTime = GameManager.instance.currentTime;

        //return currentTime.Hour >= 22 && currentTime.Hour <= 8;

        return false;
    }

    public void UseStamina(int amt)
    {
        stamina = Mathf.Clamp(stamina - amt, 0, Constants.CAP_STAMINA);
    }

    public static Pal GeneratePal()
    {
        PalID[] palPool = new PalID[]
        {
            PalID.SPROUTA,
            PalID.SPROUTB,
            PalID.SPROUTC,
            PalID.SPROUTD,
        };

        return new Pal((PalID)Random.Range(0, palPool.Length));
    }

}