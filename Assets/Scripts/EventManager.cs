﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public static class EventManager
{
    public static event Action onLoadDatabase;
    
    public static event Action<bool> onToggleCameraDrag;
    public static event Action<bool> onToggleHUDStatus;
    public static event Action<bool, bool> onToggleHUDButtons;

    public static event Action onCompleteExpedition;

    public static event Action onInitializeGarden;
    public static event Action onReturnToGarden;

    public static event Action onRefreshPalController;
    public static event Action<bool> onLoadFurniture;

    public static bool inGarden;

    // Time
    public static event Action<int> onMinutePass;
    public static event Action<int> onDayPass;

    // HUD
    public static event Action onRefreshHUD;

    // Saving
    public static event Action onLoadFile;
    public static event Action onSaveFile;

    public static event Action onNewGame;
    public static event Action onLoadGame;
    public static event Action onSaveGame;

    public static event Action<int, FurnitureID> onStartDecoration;

    public static void LoadDatabase()
    {
        onLoadDatabase?.Invoke();
    }

    public static void ToggleCameraDrag(bool active)
    {
        onToggleCameraDrag?.Invoke(active);
    }
    
    public static void InitializeGarden()
    {
        onInitializeGarden?.Invoke();
    }


    public static void CompleteExpedition()
    {
        onCompleteExpedition?.Invoke();
    }

    public static void MinutePass(int mins)
    {
        onMinutePass?.Invoke(mins);
    }

    public static void DayPass(int days)
    {
        onDayPass?.Invoke(days);
    }

    public static void NewGame()
    {
        onNewGame?.Invoke();
    }

    public static void LoadGame()
    {
        onLoadFile?.Invoke();
        onLoadGame?.Invoke();
    }

    public static void SaveGame()
    {
        onSaveFile?.Invoke();
        onSaveGame?.Invoke();
    }

    public static void RefreshHUD()
    {
        onRefreshHUD?.Invoke();
    }

    public static void ToggleHUDStatus(bool toggled)
    {
        onToggleHUDStatus?.Invoke(toggled);
    }

    public static void ToggleHUDButtons(bool toggleDeco, bool toggleMain)
    {
        onToggleHUDButtons?.Invoke(toggleDeco, toggleMain);
    }

    public static void RefreshPalController()
    {
        onRefreshPalController?.Invoke();
    }

    public static void ReturnToGarden()
    {
        onReturnToGarden?.Invoke();
    }

    public static void DecorateFurniture(int saveIndex, FurnitureID furniture)
    {
        onStartDecoration?.Invoke(saveIndex, furniture);
    }

    public static void LoadFurniture(bool reset)
    {
        onLoadFurniture?.Invoke(reset);
    }
}