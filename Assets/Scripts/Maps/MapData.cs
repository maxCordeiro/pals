﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Random = UnityEngine.Random;

public struct MapData
{
    public MapID id;
    public float timeToComplete; // In hours
    public List<LootData> loot;
}