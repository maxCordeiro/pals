using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class GameUtilityEditor : EditorWindow
{
    int category;
    string[] categoryNames =
    {
        "Time"
    };

    Vector2 scrollPos;

    // Time
    float timeScale;
    int fps;

    [MenuItem("Pals/Game Utility Editor")]
    public static void ShowWindow()
    {
        GetWindow(typeof(GameUtilityEditor));
    }

    private void OnInspectorUpdate()
    {
        Repaint();
    }

    private void OnGUI()
    {
        if (!EditorApplication.isPlaying)
        {
            GUILayout.Label("Editor not in playtest mode.");
            return;
        }

        GUIStyle styleBold = new GUIStyle
        {
            fontStyle = FontStyle.Bold,
            fontSize = 14
        };

        GUILayout.BeginHorizontal();
        for (int i = 0; i < categoryNames.Length; i++)
        {
            if (GUILayout.Button(categoryNames[i]))
            {
                Repaint();
                category = i;
            }
        }
        GUILayout.EndHorizontal();

        EditorGUILayout.BeginVertical();
        scrollPos = EditorGUILayout.BeginScrollView(scrollPos);

        switch (category)
        {
            case 0:
                timeScale = EditorGUILayout.Slider("Time Scale", timeScale, 0, 1);
                fps = EditorGUILayout.IntField("FPS", fps);

                if (GUILayout.Button("Apply Changes"))
                {
                    Time.timeScale = timeScale;
                    Application.targetFrameRate = fps;
                }

                if (GUILayout.Button("Reset"))
                {
                    Time.timeScale = 1;
                    Application.targetFrameRate = Screen.currentResolution.refreshRate;
                }

                break;

            default:
                break;
        }

        EditorGUILayout.EndScrollView();
        EditorGUILayout.EndVertical();
    }

}
